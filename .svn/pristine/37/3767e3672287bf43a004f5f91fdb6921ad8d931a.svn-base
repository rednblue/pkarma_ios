//
//  HSFacebookLoginHelper.swift
//  WantIt
//
//  Created by SOTSYS216 on 14/06/17.
//  Copyright © 2017 myCompany. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

/*
 {
     email = "accfbpost@gmail.com";
     "first_name" = Abhishree;
     id = 2251503338408075;
     "last_name" = Patel;
     picture =     {
         data =         {
             height = 320;
             "is_silhouette" = 0;
             url = "https://scontent.xx.fbcdn.net/v/t1.0-1/p320x320/14222168_2071398789751865_5802625431553854246_n.jpg?oh=a18ca613fb778891c199b28b02c284e2&oe=59DDE92E";
             width = 512;
         };
     };
 }
 */
struct SignupSocialDM {
    var email: String?
    var firstName: String?
    var lastName: String?
    var vId: String?
    
    var profilePictureURL: URL?
    
    var token: String = ""
    var userID: String = ""
}

extension SignupSocialDM{
    init(token: String, userID: String) {
        self.token = token
        self.userID = userID
    }
}

struct FBFriendListSocialDM{
    var token: String = ""
    var userID: String = ""
    var arrFriendList:[friendListDM]? = []
}

struct friendListDM {
    var vId: String?
    var name: String?
}

extension friendListDM{
    init(dict: Dictionary<String,Any>) {
        self.vId = dict["id"] as! String?
        self.name = dict["name"] as! String?
    }
}


extension FBFriendListSocialDM{
    init(token: String, userID: String) {
        self.token = token
        self.userID = userID
    }
}

typealias HSFBLoginCompletion = (_ result: SignupSocialDM?, _ isLogout: Bool, _ error: Error?)->(Void)

typealias HSFBFriendsListCompletion = (_ result: FBFriendListSocialDM?, _ nextValue: String, _ error: Error?)->(Void)


class HSFacebookLoginManager {
    
    static var manager: HSFacebookLoginManager = HSFacebookLoginManager()
    
    var readPermissions: [String] = ["email","user_friends","public_profile"]
    var loginCompletion: HSFBLoginCompletion?
    var friendListComletion: HSFBFriendsListCompletion?
    
    func initialize(application:UIApplication, launchOptions: [UIApplicationLaunchOptionsKey: Any]?, readPermissions: [String]){
        self.readPermissions = readPermissions
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool{
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func loginWithFacebook(in viewController: UIViewController, completion: @escaping HSFBLoginCompletion)
    {
        self.loginCompletion = completion
        FBSDKLoginManager().logIn(withReadPermissions: self.readPermissions, from: viewController, handler: { (result, error) in
            if error != nil
            {
                FBSDKLoginManager().logOut()
                self.loginCompletion!(nil, true, error)
                return
            }
            else if result!.isCancelled
            {
                FBSDKLoginManager().logOut()
                self.loginCompletion!(nil, true, nil)
                return
            }
            else
            {
                let param = ["fields": "id,email,first_name,last_name,picture.width(512)"]
                let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: param)
                
                graphRequest!.start { connection, result, error in
                    
                    if result != nil
                    {
                        let resultDictionary = result as! [String:Any]
                        var model = SignupSocialDM.init(token: FBSDKAccessToken.current().tokenString, userID: FBSDKAccessToken.current().userID)
                        model.email = resultDictionary["email"] as? String
                        model.vId = resultDictionary["id"] as! String?
                        model.firstName = resultDictionary["first_name"] as? String
                        model.lastName = resultDictionary["last_name"] as? String
                        model.profilePictureURL = URL(string: "https://graph.facebook.com/"+FBSDKAccessToken.current().userID+"/picture?type=large&width=720&height=720")
                        self.loginCompletion!(model, false, error)
                        return
                    }else{
                        self.loginCompletion!(nil, false, error)
                    }
                }
            }
        })
    }
    
    func getFriendList(completion: @escaping HSFBFriendsListCompletion){
//        print(FBSDKAccessToken.current())
        
        self.friendListComletion = completion

        let param = ["fields": "id,name"]
         //"me?fields=taggable_friends"
        let graphRequest = FBSDKGraphRequest(graphPath: "me/friends", parameters: param, httpMethod: "GET")
        graphRequest!.start { connection, result, error in
            
            if result != nil
            {
                let resultDictionary = result as! [String:Any]
                print(resultDictionary)
                var model = FBFriendListSocialDM.init(token: FBSDKAccessToken.current().tokenString, userID: FBSDKAccessToken.current().userID)
                
                var arr:[Dictionary<String,Any>] = []
                arr = (resultDictionary["data"] as! [Dictionary<String, Any>]?)!
                
                for i in 0..<arr.count{
                    let listModel = friendListDM.init(dict: arr[i])
                    model.arrFriendList?.append(listModel)
                }
                
//                print(model.arrFriendList!)
                
                let nextDict:[String: Any] = resultDictionary["paging"] as! [String : Any]
                if let _ = nextDict["next"]{
                    self.friendListComletion!(model, nextDict["next"] as! String, error)
                }else{
                    self.friendListComletion!(model, "", error)

                }
                return
            }else{
                self.friendListComletion!(nil, "",error)
            }
        }
    }
    

    
    
    func getNextFriendsList(next:String, completion: @escaping HSFBFriendsListCompletion){
        self.friendListComletion = completion
        
        let param = ["fields": "id,name"]
        //"me?fields=taggable_friends"
        let graphRequest = FBSDKGraphRequest(graphPath: "me/friends?after=\(next)&limit=25", parameters: param, httpMethod: "GET")
        graphRequest!.start { connection, result, error in
            
            if result != nil
            {
                let resultDictionary = result as! [String:Any]
                print(resultDictionary)
                var model = FBFriendListSocialDM.init(token: FBSDKAccessToken.current().tokenString, userID: FBSDKAccessToken.current().userID)
                
                var arr:[Dictionary<String,Any>] = []
                arr = (resultDictionary["data"] as! [Dictionary<String, Any>]?)!
                
                for i in 0..<arr.count{
                    let listModel = friendListDM.init(dict: arr[i])
                    model.arrFriendList?.append(listModel)
                }
                
                let nextDict:[String: Any] = resultDictionary["paging"] as! [String : Any]
                if let _ = nextDict["next"]{
                    self.friendListComletion!(model, nextDict["next"] as! String, error)
                }else{
                    self.friendListComletion!(model, "", error)
                }
                return
            }else{
                self.friendListComletion!(nil, "", error)
            }
        }
    }
    
   
    
}
