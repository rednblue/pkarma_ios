//
//  settingTableViewCell.swift
//  ParKarma
//
//  Created by SOTSYS022 on 02/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit

class settingTableViewCell: UITableViewCell {

    @IBOutlet var imgViewIcon: UIImageView!
    @IBOutlet var imgArrow: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewIcon.tintColor = kSKYCOLOR
        imgArrow.tintColor = UIColor.lightGray
        imgArrow.image = #imageLiteral(resourceName: "Forwrd").withRenderingMode(.alwaysTemplate)
        
        if objAppSingleton.deviceType.isSimulator{
            
            lblName.font = UIFont(name: "OpenSans-Semibold", size: 13)
        }
        else
        {
            if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_4_0_Inch) {
                lblName.font = UIFont(name: "OpenSans-Semibold", size: 13)
            }
            else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_4_7_Inch) {
                lblName.font = UIFont(name: "OpenSans-Semibold", size: 15)
            }
            else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_5_5_Inch) {
                lblName.font = UIFont(name: "OpenSans-Semibold", size: 17)
            }
            else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_5_8_Inch) {
                lblName.font = UIFont(name: "OpenSans-Semibold", size: 19)
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
