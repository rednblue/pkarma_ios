//
//  CustomInfoWindow.swift
//  CustomInfoWindow
//
//  Created by Malek T. on 12/13/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit

class CustomInfoWindow: UIView {

    @IBOutlet var imgPin: UIImageView!
    @IBOutlet var imgMsg: UIImageView!
    @IBOutlet var lblMnt: UILabel!
    
}
