//
//  ReportDM.swift
//  ParKarma
//
//  Created by SOTSYS129 on 11/06/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReportDM: BaseDM {
    
    internal let kReportKey: String = "Following Pin saved"
    internal let kUserIdKey: String = "userid"
    internal let kIconKey: String = "icon"
    internal let kLatitudeKey: String = "latitude"
    internal let kLongitudeKey: String = "longitude"
    internal let kMsgKey: String = "msg"
    internal let kTimeKey: String = "time"
    
    // MARK: Properties
    var userid: Int =  0
    var icon: Int =  0
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var msg : String = ""
    var time: Int =  0
    
    required init() {
        super.init()
    }
    required init?(jsonData:JSON){
        super.init(jsonData: jsonData)
        let json = jsonData
       // self.jsonDictReport = jsonData.dictionary
        
        if let items = json[kReportKey].dictionary {
            
            userid = (items[kUserIdKey]?.intValue)!
            icon = (items[kIconKey]?.intValue)!
            latitude = (items[kLatitudeKey]?.doubleValue)!
            longitude = (items[kLongitudeKey]?.doubleValue)!
            msg = (items[kMsgKey]?.stringValue)!
            time = (items[kTimeKey]?.intValue)!
        }
    }
    
}
