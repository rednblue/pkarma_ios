//
//  NearByPinsDM.swift
//  ParKarma
//
//  Created by SOTSYS022 on 13/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import SwiftyJSON
class NearByPinsDM: BaseDM {
    
    internal let kPinsKey: String = "pins"
    
    public var pins: [PinsDM]?
   
    required init() {
        super.init()
    }
    
    required init?(jsonData:JSON){
        super.init(jsonData: jsonData)
        let json = jsonData
        
        print(jsonData.dictionary)
        self.jsonDict = jsonData.dictionary
        pins = []
        
        if let items = json[kPinsKey].array {
            for item in items {
                pins?.append(PinsDM(jsonData: item)!)
            }
        } else {
            pins = nil
        }
    }
}
