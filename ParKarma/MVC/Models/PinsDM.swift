//
//  PinsDM.swift
//  ParKarma
//
//  Created by SOTSYS022 on 13/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import SwiftyJSON

class PinsDM: BaseDM {

    internal let kDistanceKey: String = "distance"
    internal let kIconKey: String = "icon"
    internal let kLatitudeKey: String = "latitude"
    internal let kLongitudeKey: String = "longitude"
    internal let kMsgKey: String = "msg"
    internal let kTimeKey: String = "time"
    internal let kTimeFlagKey: String = "time_flag"
    internal let kUserIdKey: String = "userid"
    
    // MARK: Properties
    var distance: Double =  0.0
    var icon: Int =  0
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var msg : String = ""
    var time: Int =  0
    var timeFlag: Int =  0
    var userid: Int =  0

    required init() {
        super.init()
    }
    
    required init?(jsonData:JSON){
        super.init(jsonData: jsonData)
        
        let json = jsonData
        
        distance = json[kDistanceKey].doubleValue
        icon = json[kIconKey].intValue
        latitude = json[kLatitudeKey].doubleValue
        longitude = json[kLongitudeKey].doubleValue
        msg = json[kMsgKey].stringValue
        time = json[kTimeKey].intValue
        timeFlag = json[kTimeFlagKey].intValue
        userid = json[kUserIdKey].intValue
    }
}
