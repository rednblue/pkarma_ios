//
//  BaseDM.swift
//  BigPlay
//
//  Created by Hitendra Mac on 28/12/16.
//  Copyright © 2016 myCompany. All rights reserved.
//

import UIKit
import SwiftyJSON
import Moya
import Moya_SwiftyJSONMapper

public protocol HSSwiftyJSONAble : ALSwiftyJSONAble {
    init()
    var jsonDict:Dictionary<String,JSON>? {get set}
}

class BaseDM: NSObject,HSSwiftyJSONAble {
    
    var jsonArray:Array<JSON>?;
    var jsonDict:Dictionary<String,JSON>?
    
    var responseCode : Bool = false
    var responseMessage : String = ""
   // var jsonDictReport : Dictionary<String,JSON>?
    var isSuccess : Bool {
        return self.responseCode
    }
    
    required override init() {
        super.init()
    }
    
    required init?(jsonData:JSON)
    {
        super.init()
       
        self.responseCode = jsonData[ApiService.Parameter.RESPONSE_STATUS].boolValue
       // self.jsonDictReport = jsonData[ApiService.Parameter.REPORT_PIN].dictionary
        self.responseMessage = jsonData[ApiService.Parameter.MESSAGE].stringValue
        self.jsonDict = jsonData[ApiService.Parameter.RESULT].dictionary
        self.jsonArray = jsonData[ApiService.Parameter.RESULT].array
//        if let dict = self.jsonDict {
////            print(dict)
//        }
//
//        if let arr = self.jsonArray {
////            print(arr)
//        }
    }
}

extension BaseDM {
    /// Maps data received from the signal into an object which implements the ALSwiftyJSONAble protocol.
    /// If the conversion fails, the signal errors.
    public func map<DMType: HSSwiftyJSONAble>(to type:DMType.Type) -> DMType {
      
        guard var mappedObject = DMType(jsonData: JSON(self.jsonDict!)) else {
            return DMType()
        }
        mappedObject.jsonDict = self.jsonDict
        return mappedObject
    }
    
    /// Maps data received from the signal into an array of objects which implement the ALSwiftyJSONAble protocol
    /// If the conversion fails, the signal errors.
    public func map<DMType: HSSwiftyJSONAble>(to type:[DMType.Type]) -> [DMType] {
        guard let mappedArray = self.jsonArray else {
            return []
        }
        let mappedObjectsArray = mappedArray.flatMap { (json) -> DMType in
            var mappedObject = DMType(jsonData: json)
            mappedObject?.jsonDict = self.jsonDict
            return mappedObject!
        }
        return mappedObjectsArray
    }
}

