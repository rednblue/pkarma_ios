//
//  UserDM.swift
//  ParKarma
//
//  Created by SOTSYS022 on 08/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserDM: BaseDM {
    
    internal let kDataEmailKey: String = "email"
    internal let kDataFirstNameKey: String = "firstName"
    internal let kDataIdKey: String = "id"
    internal let kDataLastNameKey: String = "lastName"
    internal let kDataUserNameKey: String = "user"
    internal let kDataApiKey: String = "apiKey"


    static var sharedInstance : UserDM!
   
    var userName : String = ""
    var email : String = ""
    var lastName: String = ""
    var firstName: String = ""
    var userID: Int = 0
    var apiKey : String = ""

    required init() {
        super.init()
    }
    
    required init?(jsonData:JSON){
        super.init(jsonData: jsonData)
        let json = jsonData
        self.jsonDict = jsonData.dictionary
        
        userName = json[kDataUserNameKey].stringValue
        email = json[kDataEmailKey].stringValue
        lastName = json[kDataLastNameKey].stringValue
        firstName = json[kDataFirstNameKey].stringValue
        userID = json[kDataIdKey].intValue
        apiKey = json[kDataApiKey].stringValue
    }
    
    func saveToLocal()
    {
        UserDefaults.standard.set(JSON(self.jsonDict!).rawValue, forKey: "UserDM")
        UserDefaults.standard.synchronize()
    }
    
    class func removeFromLocal(){
        
        UserDefaults.standard.set(nil, forKey: "UserDM")
        UserDefaults.standard.synchronize()
    }
    
    class func lastLoggingUser()->UserDM?
    {
        guard let userData = UserDefaults.standard.value(forKey: "UserDM") else {
            return nil
        }
        let  user = UserDM(jsonData: JSON(userData))
        return user
    }
}
