//
//  SignUpViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 31/01/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Moya

class SignUpViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet var btnFB: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignUp: UIButton!

    @IBOutlet var lblOrSignUpWith: UILabel!
    @IBOutlet var lblAlreadyUser: UILabel!
    @IBOutlet var txtEmail: customeTextField!
    @IBOutlet var txtUserName: customeTextField!
    @IBOutlet var txtPassword: customeTextField!
    @IBOutlet var txtConfirmPassword: customeTextField!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- ACTIONS
    @IBAction func btnSignUpClicked(_ sender: Any) {
        
        if String().signupValidaion(toController: self, email: txtEmail.text! , username: txtUserName.text! , password: txtPassword.text!, ConfPassword: txtConfirmPassword.text!){
           self.SignUpApi()
        }
    }
    @IBAction func btnFBClicked(_ sender: Any) {
      //  print("btnFBClicked...")
    }
    
    @IBAction func btnGoogleClicked(_ sender: Any) {
       // print("btnGoogleClicked...")
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK:- FUNCTIONS
    func setupUI() {
    
//        self.txtUserName.text = "a1"
//        self.txtEmail.text = "a1@gmail.com"
//        self.txtPassword.text = "123456"
//        self.txtConfirmPassword.text = "123456"
        
        //SET DYNAMIC FONT
        btnSignUp.titleLabel?.font = btnSignUp.titleLabel?.font.withSize((CGFloat(Int((btnSignUp.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        lblOrSignUpWith.font = lblOrSignUpWith?.font.withSize((CGFloat(Int((lblOrSignUpWith?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblAlreadyUser.font = lblAlreadyUser?.font.withSize((CGFloat(Int((lblAlreadyUser?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        txtEmail.font = txtEmail?.font?.withSize((CGFloat(Int((txtEmail?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        txtUserName.font =  txtEmail?.font
        txtPassword.font =  txtEmail?.font
        txtConfirmPassword.font =  txtEmail?.font
        
        btnFB.imageView?.contentMode = .scaleAspectFit
        btnGoogle.imageView?.contentMode =
            .scaleAspectFit
        
    }
//    func valid()
//    {
//        _ = SOValidator.ValidationMode.strict
//    }
    func SignUpApi()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            let provider = MoyaProvider<ApiService>()
            provider.request(ApiService.SignUp(user: self.txtUserName.text!, email: self.txtEmail.text!, password: self.txtPassword.text!, firstName: self.txtUserName.text!, lastName: self.txtUserName.text!), completion: { (result) in
                objAppSingleton.stopLoading()
                switch result {
                case let .success(response):
                    do {

                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)

                        print("Response-->",(response))
                        if baseModel.isSuccess{
                            
                            let alertController = UIAlertController(title: APPNAME, message: ErrorMessage.registered, preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                
                         // let loginViewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: LoginViewController.className) as! LoginViewController
                            self.navigationController?.popToRootViewController(animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            objAppSingleton.showAlert(baseModel);
                        }

//                        UserDM.sharedInstance = try response.map(to: UserDM.self)
//
//                        UserDM.sharedInstance.saveToLocal()
                        
//                        UserDM.sharedInstance = UserDM.init()
//                        UserDM.sharedInstance.email = self.txtEmail.text!
//                        UserDM.sharedInstance.userName = self.txtUserName.text!
//                        UserDM.sharedInstance.saveToLocal()

                        
//                        let objUINavigationController = UINavigationController()
//                        let objParkingSpotViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier: "ParkingSpotViewController") as! ParkingSpotViewController
//                        objUINavigationController.viewControllers = [objParkingSpotViewController]
//                        objAppDelegate.window?.rootViewController = objUINavigationController

                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })

        }
        else
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
}

