//
//  LoginViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 01/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import Moya
class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    
    @IBOutlet var btnFB: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignUp : UIButton!
    @IBOutlet var btnForgotPassword: UIButton!
    @IBOutlet var lblOrLoginWith: UILabel!
    @IBOutlet var lblNewUser: UILabel!
    
    @IBOutlet var txtEmail: customeTextField!
    @IBOutlet var txtPassword: customeTextField!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()

//        self.txtEmail.text = "mital12@gmail.com"
//        self.txtPassword.text = "123456"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- ACTIONS
    @IBAction func btnSignUpClicked(_ sender: Any) {

        self.performSegue(withIdentifier: "SignUpViewController", sender: self)
    }
    
    @IBAction func btnFBClicked(_ sender: Any) {
        print("btnFBClicked...")
        //LoginWithFB()
    }
    
    @IBAction func btnGoogleClicked(_ sender: Any) {
        print("btnGoogleClicked...")
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {

        if String().loginValidaion(toController: self, email: txtEmail.text!, password: txtPassword.text!){
            LoginApi()
        }
    }
    
    @IBAction func btnForgotPasswordClicked(_ sender: Any) {
        
        let objForgotPasswordViewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: ForgotPasswordViewController.className) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(objForgotPasswordViewController, animated: true)
        
    }
    
    // MARK:- FUNCTIONS
    func setupUI() {
        
        //HIDE NAV
        self.navigationController?.isNavigationBarHidden = true
        
        //SET DYNAMIC FONT
        btnLogin.titleLabel?.font = btnLogin.titleLabel?.font.withSize((CGFloat(Int((btnLogin.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnForgotPassword.titleLabel?.font = btnForgotPassword.titleLabel?.font.withSize((CGFloat(Int((btnForgotPassword.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        lblOrLoginWith.font = lblOrLoginWith?.font.withSize((CGFloat(Int((lblOrLoginWith?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblNewUser.font = lblNewUser?.font.withSize((CGFloat(Int((lblNewUser?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        txtEmail.font = txtEmail?.font?.withSize((CGFloat(Int((txtEmail?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        txtPassword.font =  txtEmail?.font
        
        btnFB.imageView?.contentMode = .scaleAspectFit
        btnGoogle.imageView?.contentMode =
            .scaleAspectFit
    }
    func LoginWithFB()
    {
        HSFacebookLoginManager.manager.loginWithFacebook(in: self) { (result, isLogout, error) -> (Void) in
            //            self.btnFacebook.loading = false;
            print("result:= \(String(describing: result))")
            print("isLogout:= \(isLogout)")
            print("error:= \(String(describing: error))")
            
//            objAppSingleton.startLoading()
            guard result != nil else {
                objAppSingleton.stopLoading()
                if !isLogout {
                }
                return;
            }
            if result!.email != nil{
                self.signUpWithFbAPI(resultDict: result!)
            }else{
                objAppSingleton.stopLoading()
                objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.FacebookValidation)
            }
        }
    }
    func LoginApi()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            let provider = MoyaProvider<ApiService>()
            
            provider.request(ApiService.Login( email: self.txtEmail.text!, password: self.txtPassword.text!), completion: { (result) in
                objAppSingleton.stopLoading()
                switch result {
                case let .success(response):
                    do {
                        
                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)
                        
                        print(response)
                        
                        guard baseModel.isSuccess else{
                            objAppSingleton.showAlert(baseModel);
                            return
                        }
                     
                        UserDM.sharedInstance = try response.map(to: UserDM.self)
                        UserDM.sharedInstance.saveToLocal()
                        Constant.apiKey = UserDM.sharedInstance.apiKey
                        
                        let objUINavigationController = UINavigationController()
                        let objParkingSpotViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier: ParkingSpotViewController.className) as! ParkingSpotViewController
                        objUINavigationController.viewControllers = [objParkingSpotViewController]
                        objAppDelegate.window?.rootViewController = objUINavigationController
                        
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
        
    //MARK: - Call signUpWithFbAPI
    func signUpWithFbAPI(resultDict: SignupSocialDM){
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            
            var stremail: String = ""
            if resultDict.email != nil{
                stremail = resultDict.email!
            }
            
            let provider = MoyaProvider<ApiService>()
            provider.request(ApiService.SignUp(user: "\(resultDict.firstName!) \(resultDict.lastName!)", email: stremail, password: "", firstName: "\(resultDict.firstName!)", lastName: "\(resultDict.lastName!)"), completion: { (result) in
                objAppSingleton.stopLoading()
                switch result {
                case let .success(response):
                    do {
                        
                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)
                        
                        print(response)
                        objAppSingleton.stopLoading()

                        if baseModel.isSuccess{
                            let alertController = UIAlertController(title: APPNAME, message: baseModel.responseMessage, preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                let objLoginViewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: LoginViewController.className) as! LoginViewController
                                self.navigationController?.pushViewController(objLoginViewController, animated: true)
                                
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            objAppSingleton.showAlert(baseModel);
                        }
                        
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
}

