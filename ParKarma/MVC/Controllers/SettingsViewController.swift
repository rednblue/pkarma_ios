//
//  SettingsViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 02/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import MessageUI

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate {
    
    //MARK:- IBOutlets
    
    @IBOutlet var tblSettings: UITableView!
    @IBOutlet var viewLogout: UIView!
    @IBOutlet var imgLogout: UIImageView!
    @IBOutlet var lblLogout: UILabel!
    @IBOutlet var lblSubMsg: UILabel!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setupUI()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    //MARK:- ACTIONS
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogoutYesClicked(_ sender: Any) {
        
        viewLogout.isHidden = true
        UserDM.removeFromLocal()
        let objLoginViewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: LoginViewController.className) as! LoginViewController
        let navController : UINavigationController = UINavigationController(rootViewController: objLoginViewController)
       // navController.addChildViewController(objLoginViewController) 
        navController.navigationBar.isHidden = true
        objAppDelegate.window?.rootViewController = navController
        Constant.apiKey = nil
    }
    
    @IBAction func btnLogoutNoClicked(_ sender: Any) {
        
        viewLogout.isHidden = true
    }
    
    // MARK: - FUNCTIONS
    func setupUI() {
        setNav()
        
        //SET DYNAMIC FONT
        btnYes.titleLabel?.font = btnYes.titleLabel?.font.withSize((CGFloat(Int((btnYes.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnNo.titleLabel?.font = btnNo.titleLabel?.font.withSize((CGFloat(Int((btnNo.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblLogout.font = lblLogout?.font.withSize((CGFloat(Int((lblLogout?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblSubMsg.font = lblSubMsg?.font.withSize((CGFloat(Int((lblSubMsg?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        tblSettings.tableFooterView = UIView()
        tblSettings.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblSettings.frame.size.width, height: 1))
        viewLogout.isHidden = true
        imgLogout.tintColor = kSKYCOLOR
        imgLogout.image = #imageLiteral(resourceName: "logout").withRenderingMode(.alwaysTemplate)
        
    }
    
    func setNav()
    {
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
        self.navigationItem.title = "Settings"
      
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    // MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSettings.dequeueReusableCell(withIdentifier:settingTableViewCell.className, for: indexPath) as! settingTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.lblName.text = "Change Password"
            cell.imgViewIcon.image = #imageLiteral(resourceName: "ChangePassword").withRenderingMode(.alwaysTemplate)
            
        case 1:
            cell.lblName.text = "Feedback"
            cell.imgArrow.isHidden = true
            cell.imgViewIcon.image = #imageLiteral(resourceName: "feedback").withRenderingMode(.alwaysTemplate)
            
        case 2:
            cell.lblName.text = "Logout"
            cell.imgViewIcon.image = #imageLiteral(resourceName: "logout").withRenderingMode(.alwaysTemplate)
            cell.imgArrow.isHidden = true
            
        default:
            break
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let objchnagePasswordViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier: chnagePasswordViewController.className) as! chnagePasswordViewController
            self.navigationController?.pushViewController(objchnagePasswordViewController, animated: true)
            break
        case 1:
             self.presentMailComposer()
//            let objFeedbackViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier:FeedbackViewController.className) as! FeedbackViewController
//            self.navigationController?.pushViewController(objFeedbackViewController, animated: true)
            break
        case 2:
            viewLogout.isHidden = false
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func presentMailComposer(){
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
        else{
            showSendMailErrorAlert()
        }
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@tourtempo.com"])
        mailComposerVC.setSubject("Parkarma Feedback")
//        mailComposerVC.setCcRecipients([UserDefaults.standard.value(forKey: "emailID") as! String])
        //ailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    func showSendMailErrorAlert() {
        
        let alertController = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail, Please check e-mail configuration and try again.", preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
