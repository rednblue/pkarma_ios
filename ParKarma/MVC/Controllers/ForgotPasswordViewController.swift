//
//  ForgotPasswordViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 01/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import Moya

class ForgotPasswordViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet var btnBackToLogin: UIButton!
    @IBOutlet var lblForgotPassword: UILabel!
    @IBOutlet var txtEmail: customeTextField!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var btnSubmit: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupUI() {
        
        //SET DYNAMIC FONT
        btnBackToLogin.titleLabel?.font = btnBackToLogin.titleLabel?.font.withSize((CGFloat(Int((btnBackToLogin.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnSubmit.titleLabel?.font = btnSubmit.titleLabel?.font.withSize((CGFloat(Int((btnSubmit.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        lblForgotPassword.font = lblForgotPassword?.font.withSize((CGFloat(Int((lblForgotPassword?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblMessage.font = lblMessage?.font.withSize((CGFloat(Int((lblMessage?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        txtEmail.font = txtEmail?.font?.withSize((CGFloat(Int((txtEmail?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        btnBackToLogin.imageView?.contentMode = .scaleAspectFit
        btnBackToLogin.sizeToFit()
    }
    
    //MARK: - ACTIONS
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if String().forgotPasswordValidation(toController: self, email: txtEmail.text!){
            ForgotPasswordApi()
        }
    }
    @IBAction func btnBackToLoginClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- FUNCTIONS
    func ForgotPasswordApi()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            let provider = MoyaProvider<ApiService>()
            provider.request(ApiService.ForgotPassword( email: self.txtEmail.text!), completion: { (result) in
                objAppSingleton.stopLoading()
                switch result {
                case let .success(response):
                    do {
                        
                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)
                        
                        print(response)
                        if baseModel.isSuccess{
                            let alertController = UIAlertController(title: APPNAME, message: baseModel.responseMessage, preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            objAppSingleton.showAlert(baseModel);
                        }
                        
                        
                        
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
}
