//
//  FeedbackViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 02/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextViewDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var imgViewIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtViewMsg: customeTextView!
    @IBOutlet var lblCharCount: UILabel!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        
        setNav()
        
        //SET DYNAMIC FONT
        lblTitle.font = lblTitle?.font.withSize((CGFloat(Int((lblTitle?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblCharCount.font = lblCharCount?.font.withSize((CGFloat(Int((lblCharCount?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        txtViewMsg.font = txtViewMsg?.font?.withSize((CGFloat(Int((txtViewMsg?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnSend.titleLabel?.font = btnSend.titleLabel?.font.withSize((CGFloat(Int((btnSend.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        imgViewIcon.tintColor = kSKYCOLOR
        imgViewIcon.image = #imageLiteral(resourceName: "ChangePassword").withRenderingMode(.alwaysTemplate)
        
        imgViewIcon.tintColor = kSKYCOLOR
        imgViewIcon.image = #imageLiteral(resourceName: "feedback").withRenderingMode(.alwaysTemplate)
        
        txtViewMsg.delegate = self
        
    }
    
    func setNav()
    {
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
        self.navigationItem.title = "Settings"
        
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    // MARK: - ACTIONS
    
    @IBAction func btnSendClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITextView Delegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewMsg.text.trim() == LabelDefaultText.feedbackPlaceHolder{
           txtViewMsg.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewMsg.text.trim() == "" {
            txtViewMsg.text = LabelDefaultText.feedbackPlaceHolder
            txtViewMsg.textColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        if updatedText.count <= TextViewMaxChar{
            lblCharCount.text = ("\(updatedText.count)/\(TextViewMaxChar) characters")}
        return updatedText.count <= TextViewMaxChar
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        if changedText.count <= TextViewMaxChar{
            lblCharCount.text = ("\(changedText.count)/\(TextViewMaxChar) characters")}
        return changedText.count <= TextViewMaxChar
    }
}
