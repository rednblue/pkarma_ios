//
//  chnagePasswordViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 02/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import Moya
class chnagePasswordViewController: UIViewController {
    
    //MARK:- IBOutlets
    
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var imgViewIcon: UIImageView!
    @IBOutlet var lblTitleName: UILabel!
    @IBOutlet var txtOldPassword: customeTextField!
    @IBOutlet var txtNewPassword: customeTextField!
    @IBOutlet var txtConfirmPassword: customeTextField!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func changePasswordAPI()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            let provider = MoyaProvider<ApiService>()
            provider.request(ApiService.ChangePassword(email: UserDM.sharedInstance.email, oldPassword: self.txtOldPassword.text!, newPassword: self.txtNewPassword.text!), completion: { (result) in
                objAppSingleton.stopLoading()
                switch result {
                case let .success(response):
                    do {
                        objAppSingleton.stopLoading()
                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)
                        
                        print(response)
                        
                        if baseModel.isSuccess{
                            let alertController = UIAlertController(title: APPNAME, message: baseModel.responseMessage, preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            objAppSingleton.showAlert(baseModel);
                        }
                        
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })
            
        }
        else
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
    
    func setupUI() {
        
        setNav()
        
        //SET DYNAMIC FONT
        lblTitleName.font = lblTitleName?.font.withSize((CGFloat(Int((lblTitleName?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        txtOldPassword.font = txtOldPassword?.font?.withSize((CGFloat(Int((txtOldPassword?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        txtNewPassword.font = txtOldPassword.font
        txtConfirmPassword.font = txtOldPassword.font
        
        btnSave.titleLabel?.font = btnSave.titleLabel?.font.withSize((CGFloat(Int((btnSave.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        imgViewIcon.tintColor = kSKYCOLOR
        imgViewIcon.image = #imageLiteral(resourceName: "ChangePassword").withRenderingMode(.alwaysTemplate)
        
    }
    func setNav()
    {
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnBack.addTarget(self, action: #selector(self.btnBackClicked(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnBack)
        self.navigationItem.setLeftBarButton(item1, animated: true)
        self.navigationItem.title = "Settings"
        
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    // MARK: - ACTIONS
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveClicked(_ sender: Any) {
        if valid()
        {
            changePasswordAPI()
            //self.navigationController?.popViewController(animated: true)
        }
    }
    func valid()->Bool
    {
        _ = SOValidator.ValidationMode.strict
        if(!SOValidator.required(txtOldPassword!.text!))
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NOldPasswordEmpty)
            return false
        }
        
        if(!SOValidator.required(txtNewPassword!.text!))
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NNewPasswordEmpty)
            return false
        }
        
        if !((self.txtNewPassword.text?.length)! > 5 && (self.txtNewPassword.text?.length)! < 32) {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NInvalidPassword)
            return false
        }
//        //remove validation for contain oneNumber and Capital latter
//        if !objAppSingleton.isConatinsOneNumber(passwordString: self.txtNewPassword.text!)
//        {
//            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NInvalidPassword)
//            return false
//        }
//        if !objAppSingleton.isConatinsOneCapitalLetter(passwordString: self.txtNewPassword.text!)
//        {
//            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NInvalidPassword)
//            return false
//        }
        if(!SOValidator.required(txtConfirmPassword!.text!))
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NConfirmPasswordEmpty)
            return false
        }
        
        if(self.txtNewPassword.text != self.txtConfirmPassword.text){
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NMatchPassword)
            return false
        }
        return true
    }
    
}

extension UITextField {
    @IBInspectable var padding_left: CGFloat {
        get {
            return 0
        }
        set (f) {
            layer.sublayerTransform = CATransform3DMakeTranslation(f, 0, 0)
        }
    }
}
