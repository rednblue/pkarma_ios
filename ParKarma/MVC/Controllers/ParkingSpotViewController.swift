//
//  ParkingSpotViewController.swift
//  ParKarma
//
//  Created by SOTSYS022 on 01/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import Moya
import GoogleMaps
import SwiftyJSON



class ParkingSpotViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,UITextViewDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet var btnReportParkingSpot: UIButton!
    
    // Confirmation view outlets
    @IBOutlet var viewConfimation: UIView!
    @IBOutlet var btnCancelConfirmView: UIButton!
    @IBOutlet var lblMsgConfirmView: UILabel!
    @IBOutlet var lblSubMsgConfirmView: UILabel!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var btnSendAMsg: UIButton!
    @IBOutlet weak var subViewAboutNote: UIView!
    @IBOutlet weak var subviewAddNote: UIView!
    
    // Confirmation AddNote outlets
    @IBOutlet var viewAddNote: UIView!
    @IBOutlet var lblAddNote: UILabel!
    @IBOutlet var txtViewTypeMsg: customeTextView!
    @IBOutlet var lblMsgCharCount: UILabel!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnCancelAddNoteView: UIButton!
    @IBOutlet var imgViewAddNoteIcon: UIImageView!
    
    // Confirmation AboutSpot outlets
    @IBOutlet var viewAboutSpot: UIView!
    @IBOutlet var lblAboutSpotNote: UILabel!
    @IBOutlet var txtViewTypeMsgAboutSpot: UITextView!
    @IBOutlet var btnAboutSpotOk: UIButton!
    @IBOutlet var imgViewAboutSpotNoteIcon: UIImageView!
    @IBOutlet var viewGMSView: GMSMapView!
    /*@IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var slider: UISlider!{
        didSet{
            
            slider.setThumbImage(#imageLiteral(resourceName: "naviagtion"), for: .normal)
            slider.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.5)
            slider.transform = slider.transform.rotated(by: CGFloat(0.5 * Float.pi))
            slider.trackRect(forBounds: CGRect(x: 0, y: 0, width: 10, height: 50))
            slider.maximumTrackTintColor = UIColor.black
            slider.minimumTrackTintColor = UIColor.gray
            slider.setMinimumTrackImage(#imageLiteral(resourceName: "grayImage"), for: .normal)
            slider.setMaximumTrackImage(#imageLiteral(resourceName: "blackLine"), for: .normal)
            slider.minimumValue = 0.1
            slider.maximumValue = 2
           
        }
    }*/
    var locationManager = CLLocationManager()
    var myCurrLocation = CLLocation()
    var savePosition = CLLocationCoordinate2D()
    var arrMarker = [GMSMarker]()
    var arrNearByPins:[PinsDM] = []
    var reportDMObj = ReportDM()
    var pinObject = PinsDM()
    var txtViewString : String = ""
    var latitude = Double()
    var logitude = Double()
    var flagMapReload : Bool = false
     let marker = GMSMarker()

    var zoomLavel : Float = 13.0
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
        //checkPermission()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func applicationDidBecomeActive(){
        
        if !isLocationServiceEnabled(){
            ShowPermissionAlert()
        }
    }

    func ShowPermissionAlert(){
        
            let alertController = UIAlertController(title: APPNAME, message: AlertMessage.locationPermission, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Turn on location service", style: .default, handler: { (alert) in
                UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
            }))
            
            OperationQueue.main.addOperation {
                self.present(alertController, animated: true,
                             completion:nil)
            }
    }
    
    //MARK:- ACTIONS
    
    /*@IBAction func sliderValueChange(_ sender: UISlider) {
        
        // Restrict slider to a fixed value
        
        let fixed = (roundf(sender.value / 0.05) * 0.05);
          print(sender.value,"-------",fixed)
        sender.setValue(fixed, animated: true)
        
        lblMiles.text = "\(Float(fixed)) MI"
    }
    @IBAction func sliderValueEndEditing(_ sender: UISlider) {
        
        Constant.milesValue = (roundf(sender.value / 0.05) * 0.05)
        self.GetNearByPinsApi()

    }*/
    @IBAction func btnBackClicked(_ sender: Any) {
        print("btnBackClicked...")
    }
    @IBAction func btnSettingClicked(_ sender: Any) {
        
        let objSettingsViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier: SettingsViewController.className) as! SettingsViewController
        self.navigationController?.pushViewController(objSettingsViewController, animated: true)
    }
    @IBAction func btnReportParkingSpotClicked(_ sender: Any) {
        print("btnReportParkingSpotClicked...")
        
        objAppSingleton.setView(view: self.viewConfimation, subView: subViewAboutNote, hidden: false)
     
    }
    @IBAction func btnCancelCVClicked(_ sender: Any) {
        print("btnCancelCVClicked...")
        
        objAppSingleton.hideView(view: viewConfimation)
        
    }
    @IBAction func btnConfirmClicked(_ sender: Any) {
  
        if txtViewTypeMsg.text.trim() != "" && txtViewTypeMsg.text != LabelDefaultText.textViewPlaceHolder{
           
            txtViewString = txtViewTypeMsg.text
        }else{
           txtViewString = ""
        }
        self.ReportParkingPinApi()
        
    }
    @IBAction func btnSendAMsgClicked(_ sender: Any) {
        print("btnSendAMsgClicked...")
        
        objAppSingleton.hideView(view: viewConfimation)
//        objAppSingleton.hideView(view: viewAddNote)
        objAppSingleton.setView(view: viewAddNote, subView: subviewAddNote, hidden: false)
    }
    @IBAction func btnCancelANVClicked(_ sender: Any) {
        print("btnCancelANVClicked...")
       
        objAppSingleton.hideView(view: viewAddNote)
    }
    @IBAction func btnAddANVClicked(_ sender: Any) {
      
        if txtViewTypeMsg.text == LabelDefaultText.textViewPlaceHolder
        {
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NAddSpotNoteEmpty)
        }
        else{
            if txtViewTypeMsg.text.trim() != ""{
                
                txtViewString = txtViewTypeMsg.text
            }else{
                txtViewString = ""
            }
            self.ReportParkingPinApi()
        }
    }
    @IBAction func btnOkABNVClicked(_ sender: Any) {
        objAppSingleton.hideView(view: viewAboutSpot)
    }
    
    // MARK:- FUNCTIONS
    func setupUI() {
        
        locationManager.delegate = self
        
        Constant.milesValue = 2.0
        latitude = myCurrLocation.coordinate.latitude
        logitude = myCurrLocation.coordinate.longitude
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(myCurrLocation.coordinate.latitude), longitude: Double(myCurrLocation.coordinate.longitude) , zoom: 10.0)
        self.viewGMSView.camera = camera
    
        //check location manager permission while application becomeActive in this Controller
        NotificationCenter.default.addObserver(self,selector: #selector(applicationDidBecomeActive),                                               name: .UIApplicationWillEnterForeground,object: nil)
        
        self.setNav()
        
        //HIDE POPUPS
        self.viewConfimation.isHidden = true
        self.viewAddNote.isHidden = true
        self.viewAboutSpot.isHidden = true
        txtViewTypeMsg.delegate = self
        
        imgViewAddNoteIcon.tintColor = kSKYCOLOR
        imgViewAddNoteIcon.image = #imageLiteral(resourceName: "add_note").withRenderingMode(.alwaysTemplate)
        imgViewAboutSpotNoteIcon.tintColor = kSKYCOLOR
        imgViewAboutSpotNoteIcon.image = #imageLiteral(resourceName: "page").withRenderingMode(.alwaysTemplate)
        btnReportParkingSpot.imageView?.contentMode = .scaleAspectFit
        
        //SET DYNAMIC FONT
        btnReportParkingSpot.titleLabel?.font = btnReportParkingSpot.titleLabel?.font.withSize((CGFloat(Int((btnReportParkingSpot.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnCancelConfirmView.titleLabel?.font = btnCancelConfirmView.titleLabel?.font.withSize((CGFloat(Int((btnCancelConfirmView.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnCancelAddNoteView.titleLabel?.font = btnCancelAddNoteView.titleLabel?.font.withSize((CGFloat(Int((btnCancelAddNoteView.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnAdd.titleLabel?.font = btnAdd.titleLabel?.font.withSize((CGFloat(Int((btnAdd.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnAboutSpotOk.titleLabel?.font = btnAboutSpotOk.titleLabel?.font.withSize((CGFloat(Int((btnAboutSpotOk.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnConfirm.titleLabel?.font = btnConfirm.titleLabel?.font.withSize((CGFloat(Int((btnConfirm.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        btnSendAMsg.titleLabel?.font = btnSendAMsg.titleLabel?.font.withSize((CGFloat(Int((btnSendAMsg.titleLabel?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        lblMsgConfirmView.font = lblMsgConfirmView?.font.withSize((CGFloat(Int((lblMsgConfirmView?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblSubMsgConfirmView.font = lblSubMsgConfirmView?.font.withSize((CGFloat(Int((lblSubMsgConfirmView?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblAddNote.font = lblAddNote?.font.withSize((CGFloat(Int((lblAddNote?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblMsgCharCount.font = lblMsgCharCount?.font.withSize((CGFloat(Int((lblMsgCharCount?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        lblAboutSpotNote.font = lblAboutSpotNote?.font.withSize((CGFloat(Int((lblAboutSpotNote?.font.pointSize)!)+objAppSingleton.additionalFontSize)))
        
        txtViewTypeMsg.font = txtViewTypeMsg?.font?.withSize((CGFloat(Int((txtViewTypeMsg?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
        txtViewTypeMsgAboutSpot.font = txtViewTypeMsgAboutSpot?.font?.withSize((CGFloat(Int((txtViewTypeMsgAboutSpot?.font?.pointSize)!)+objAppSingleton.additionalFontSize)))
    }
    
    func setNav()
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        let btnSettings = UIButton(type: .custom)
        btnSettings.setImage(UIImage(named: "settings"), for: .normal)
        btnSettings.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnSettings.addTarget(self, action: #selector(self.btnSettingClicked(_:)), for: .touchUpInside)
        let itemSettings = UIBarButtonItem(customView: btnSettings)
        self.navigationItem.setRightBarButton(itemSettings, animated: true)

        let button = UIButton(type: .custom)
        let barButtonItem = UIBarButtonItem(customView: button)

//        let button2 = UIButton(type: .custom)
//        button2.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0)
//        let barButtonItem2 = UIBarButtonItem(customView: button2)

        let button3 = UIButton(type: .custom)
        button3.setImage(UIImage (named: "appLogoBlack"), for: .normal)
        button3.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0)
        button3.isUserInteractionEnabled = false
        let barButtonItem3 = UIBarButtonItem(customView: button3)
        self.navigationItem.leftBarButtonItems = [barButtonItem,barButtonItem3]//, barButtonItem2
        self.navigationItem.title = "Good Parking Karma"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "OpenSans", size: 17)!]
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    //MARK:- API calling getPins

    @objc func GetNearByPinsApi()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            let provider = MoyaProvider<ApiService>()
            
            savePosition.latitude = self.latitude
            savePosition.longitude = self.logitude
            
//            var random: CGFloat = CGFloat(1 + arc4random_uniform(9))
//            random = random / 100000000;
            print("get near by pins api ",self.latitude, self.logitude)
            
//            self.latitude = 41.8788864
//            self.logitude = -87.6381089
            //41.8788764&longitude=-87.6381089 //uncommet when live
            
            provider.request(ApiService.getNearByPins(user_id: UserDM.sharedInstance.userID, latitude: self.latitude, longitude: self.logitude, icon: 1, distance: Constant.milesValue!)
                , completion: { (result) in
                objAppSingleton.stopLoading()
                    
                switch result {
                case let .success(response):
                    do {
                        print("response.statusCode--->",response.statusCode)
                        if response.statusCode == 404 {
                            return
                        }
                        let baseModel = try response.map(to: BaseDM.self)
                         objAppSingleton.stopLoading()
                         print(response)
                        
                        if baseModel.isSuccess{
                            
                            let localBaseDM = BaseDM()
                            localBaseDM.jsonArray = try! JSON(response.mapJSON())["pins"].array
                            localBaseDM.jsonDict = try! JSON(response.mapJSON())["pins"].dictionary
                            
                            if let _ = localBaseDM.jsonDict {
                                self.arrNearByPins = [localBaseDM.map(to: PinsDM.self)]
                            }else if let _ = localBaseDM.jsonArray {
                                self.arrNearByPins = localBaseDM.map(to: [PinsDM.self])
                            }
                            
                            print("Mital:= self.arrNearByPins.count := \(self.arrNearByPins.count)")
                            
                            //self.arrNearByPins = (try response.map(to: NearByPinsDM.self).pins)!
                            self.loadMarkers()

                        }else{
                            if response.statusCode == 400{
                                objAppSingleton.goToLoginScreen()
                            }else{
                            objAppSingleton.showAlert(baseModel)
                            }
                        }
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                }
            })
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
    //MARK:- API calling ReportParking

    func ReportParkingPinApi()
    {
        if objAppSingleton.isNetwork()
        {
            objAppSingleton.startLoading()
            
            let provider = MoyaProvider<ApiService>()
            print("Last Latitude \(myCurrLocation.coordinate.latitude) Longitude \(myCurrLocation.coordinate.longitude)")
            let currentLatitude = myCurrLocation.coordinate.latitude
            let currentLongitude = myCurrLocation.coordinate.longitude
            
            provider.request(ApiService.reportParkingPin(user_id: UserDM.sharedInstance.userID, latitude:currentLatitude , longitude:currentLongitude, icon: 1, distance: Int(Constant.milesValue!) ,msg: txtViewString),
                             //Double(myCurrLocation.coordinate.latitude)
                             //latitude Double(myCurrLocation.coordinate.longitude)
             completion: { (result) in
                objAppSingleton.stopLoading()
                objAppSingleton.hideView(view: self.viewConfimation)
                objAppSingleton.hideView(view: self.viewAddNote)
                
                switch result {
                case let .success(response):
                    do {
                        let baseModel = try response.map(to: BaseDM.self)
                        print("response.statusCode--->",response.statusCode)
                        if response.statusCode == 404 {
                            return
                        }
                        print(response)
                        
                        if baseModel.isSuccess{
                            let alertController = UIAlertController(title: APPNAME, message: ErrorMessage.NParkingReportedSuccess, preferredStyle: UIAlertControllerStyle.alert)
                            
                            let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                
                                self.txtViewTypeMsg.text = LabelDefaultText.textViewPlaceHolder
                            }
                            
                            self.reportDMObj = (try response.map(to: ReportDM.self))
                            self.pinObject.latitude = self.reportDMObj.latitude
                            self.pinObject.longitude = self.reportDMObj.longitude
                            self.pinObject.time = self.reportDMObj.time
                            self.pinObject.icon = self.reportDMObj.icon
                            self.pinObject.msg = self.reportDMObj.msg
                            self.pinObject.userid = self.reportDMObj.userid
                            self.arrNearByPins.append(self.pinObject)
//                            self.loadMarkers()
                            self.afterReportPinDrop()
                            
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            if response.statusCode == 400{
                                objAppSingleton.goToLoginScreen()
                            }else{
                                objAppSingleton.showAlert(baseModel);
                            }
                        }
                    } catch {
                        print(error)
                    }
                case let .failure(error):
                    print(error)
                    
                }
            })
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NETWORKALERT)
        }
    }
    //MARK:- loadMarks

    @IBAction func currentLocClicked(_ sender: UIButton) {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        let camera = GMSCameraPosition.camera(withLatitude: Double(myCurrLocation.coordinate.latitude), longitude: Double(myCurrLocation.coordinate.longitude) , zoom: 10.0)
        self.viewGMSView.camera = camera

        
    }
    func afterReportPinDrop() {
        
        viewGMSView.clear()
        let usermarker = GMSMarker()
        usermarker.position = CLLocationCoordinate2D(latitude: Double(myCurrLocation.coordinate.latitude+0.000002), longitude:  Double(myCurrLocation.coordinate.longitude+0.000002))
        
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)![0] as! CustomInfoWindow
        customInfoWindow.imgMsg.isHidden = true
        customInfoWindow.lblMnt.isHidden = true
        usermarker.iconView = customInfoWindow
        usermarker.isTappable = false
        
        usermarker.appearAnimation = .pop
        usermarker.map = self.viewGMSView
        
        //add marker
        for pin in arrNearByPins
        {
            addMarker(pinData:pin)
        }
        self.viewGMSView.delegate = self
        
        var bounds = GMSCoordinateBounds()
        for marker in arrMarker{
            let zooming =   (zoomLavel + 2.0 - 2.0 )
            let camera = GMSCameraPosition.camera(withLatitude: Double(self.myCurrLocation.coordinate.latitude), longitude: Double(self.myCurrLocation.coordinate.longitude) , zoom:zooming)
            self.viewGMSView.camera = camera
            bounds = bounds.includingCoordinate(marker.position)
            GMSCameraUpdate.fit(bounds, withPadding: 100.0)
        }
        
    }
    
    func loadMarkers() {
        
        viewGMSView.clear()
        let usermarker = GMSMarker()
        usermarker.position = CLLocationCoordinate2D(latitude: Double(myCurrLocation.coordinate.latitude+0.000002), longitude:  Double(myCurrLocation.coordinate.longitude+0.000002))
        
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)![0] as! CustomInfoWindow
       // customInfoWindow.imgPin.image = #imageLiteral(resourceName: "currentLoc")
        customInfoWindow.imgMsg.isHidden = true
        customInfoWindow.lblMnt.isHidden = true
        usermarker.iconView = customInfoWindow
        usermarker.isTappable = false
       
        usermarker.appearAnimation = .pop
        usermarker.map = self.viewGMSView
       
        //add marker
        for pin in arrNearByPins
        {
           addMarker(pinData:pin)
        }
        self.viewGMSView.delegate = self
        
        var bounds = GMSCoordinateBounds()
        for marker in arrMarker{
          
            let zooming =   (zoomLavel + 2.0 - 2.0 )//(max Slider Value 2.0) //(zoomLavel * 2.0 / slider.value - 2.0 )
            let camera = GMSCameraPosition.camera(withLatitude: Double(self.latitude), longitude: Double(self.logitude) , zoom:zooming)
            
            
            
            self.viewGMSView.camera = camera
            
            bounds = bounds.includingCoordinate(marker.position)
            GMSCameraUpdate.fit(bounds, withPadding: 100.0)

//           GMSCameraUpdate.fit(bounds, withPadding: 100)
//           self.viewGMSView.animate(with: update)
        }

    }
    //add marker
    func addMarker(pinData:PinsDM)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: pinData.latitude, longitude:  pinData.longitude)

        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)![0] as! CustomInfoWindow
        customInfoWindow.imgPin.tintColor = objAppSingleton.getPinColor(colorId: pinData.timeFlag)
        customInfoWindow.imgPin.image = #imageLiteral(resourceName: "pin").withRenderingMode(.alwaysTemplate)
        
        if pinData.msg.trim() != ""{

             customInfoWindow.imgMsg.backgroundColor = objAppSingleton.getMsgColor(colorId: pinData.timeFlag)
             customInfoWindow.imgMsg.isHidden = false
        }else{
            customInfoWindow.imgMsg.isHidden = true
        }
        
//      customInfoWindow.lblMnt.text = String(describing: Float(pinData.distance).roundToInt())+"m"
        if Double(pinData.distance) > 0{
            customInfoWindow.lblMnt.text = String(format: "%.1fm", Double(pinData.distance))
        }
       
        marker.iconView = customInfoWindow
        marker.title = pinData.msg
        marker.map = self.viewGMSView
       
        //uncomment 2 lines

        arrMarker.append(marker)
    }
    func stringToDate(_ str: Int)->Date{
     
        let date = Date.init(timeIntervalSince1970: TimeInterval(str))
        
        //formatter.dateFormat="yyyy-MM-dd hh:mm:ss Z"
        return date
    }
    //MARK:- GMSMapView Delegates
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {

//        let index = arrMarker.index(where: {$0 === marker})
//        arrMarker[index!].tracksInfoWindowChanges = true
        
        UIView.animate(withDuration: 0.2, animations: {
            marker.iconView?.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            
        }) { (finished) in
            UIView.animate(withDuration: 0.1, animations: {
                marker.iconView?.transform = CGAffineTransform.identity
                if  marker.title?.trim() != ""{
                    self.txtViewTypeMsgAboutSpot.text = marker.title
                    self.txtViewTypeMsgAboutSpot.textColor = UIColor.darkGray
                    self.txtViewTypeMsgAboutSpot.backgroundColor = UIColor.white
                    objAppSingleton.setView(view: self.viewAboutSpot,subView: self.subViewAboutNote, hidden: false)
                   // return true
                }else{
                   // return false
                }
            })
        }
        return true
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
         print(position)
        //myCurrLocation = position.target//locationManager.location!
    }
    func mapView(_ mapView:GMSMapView,idleAt position:GMSCameraPosition)
    {
        print("\(position.target.latitude)---> \(position.target.longitude)")
        
        if savePosition != nil{
            let loc1 = CLLocation(latitude:(savePosition.latitude), longitude: (savePosition.longitude))
            let loc2 = CLLocation(latitude: (position.target.latitude), longitude: (position.target.longitude))
            let distance = loc1.distance(from: loc2)
            let miles = distance/1609.344
            print("Miles -->",miles,"\(position.target.latitude) --> \(position.target.longitude)")
            if miles > 2.0 {//Double(UserDM.sharedUser.mapRadius)
                self.latitude = position.target.latitude
                self.logitude = position.target.longitude
                self.GetNearByPinsApi()
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        objAppSingleton.setView(view: self.viewAboutSpot,subView: self.subViewAboutNote, hidden: false)
    }
    //MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         // Vishva - Start
         myCurrLocation = locationManager.location!
         // Vishva - End
          self.latitude = (locationManager.location?.coordinate.latitude)!
          self.logitude = (locationManager.location?.coordinate.longitude)!
          self.locationManager.stopUpdatingLocation()
    }
    
//    func locationManager( _ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        self.latitude = (locationManager.location?.coordinate.latitude)!
//        //        self.logitude = (locationManager.location?.coordinate.longitude)!
//        let location = locations.last
//
//        let camera = GMSCameraPosition.cameraWithLatitude((location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
//
//        self.mapView?.animateToCameraPosition(camera)
//
//        //Finally stop updating location otherwise it will come again and again in this delegate
//        self.locationManager.stopUpdatingLocation()
//
//    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            self.perform(#selector(self.GetNearByPinsApi), with: self, afterDelay: 2.0)
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            self.perform(#selector(self.GetNearByPinsApi), with: self, afterDelay: 2.0)
            break
        case .restricted:
                ShowPermissionAlert()
                break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            ShowPermissionAlert()
            break
        }
    }
    //MARK:- UITextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtViewTypeMsg.text == LabelDefaultText.textViewPlaceHolder{
            txtViewTypeMsg.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewTypeMsg.text.trim() == "" {
            txtViewTypeMsg.text = LabelDefaultText.textViewPlaceHolder
            txtViewTypeMsg.textColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
         if updatedText.count <= TextViewMaxChar{
            lblMsgCharCount.text = ("\(updatedText.count)/\(TextViewMaxChar) characters")}
        return updatedText.count <= TextViewMaxChar
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        if changedText.count <= TextViewMaxChar{
            lblMsgCharCount.text = ("\(changedText.count)/\(TextViewMaxChar) characters")}
        return changedText.count <= TextViewMaxChar
    }
}

