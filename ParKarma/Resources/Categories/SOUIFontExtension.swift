//
//  SOUIFontExtension.swift
//  Firefighter
//
//  Created by SOTSYS216 on 21/06/16.
//  Copyright © 2016 myCompany. All rights reserved.
//

import Foundation
import UIKit



extension UIFont {
    static var delatFontSize : CGFloat {
        return 0.0
    }
    
    class func font_semibold(_ size : CGFloat) -> UIFont {
//        return UIFont(name: "Roboto-Medium", size: size+delatFontSize)!;
        return UIFont(name: "OpenSans-Semibold", size: size+delatFontSize)!;
    }
    
    class func font_light(_ size : CGFloat) -> UIFont {
//        return UIFont(name: "Roboto-Light", size: size+delatFontSize)!;
        return UIFont(name: "OpenSans-Light", size: size+delatFontSize)!;
    }
    
    class func font_regular(_ size : CGFloat) -> UIFont {
//        return UIFont(name: "Roboto-Regular", size: size+delatFontSize)!;
        return UIFont(name: "OpenSans-Regular", size: size)!;
    }
    
    class func font_bold(_ size : CGFloat) -> UIFont {
//        return UIFont(name: "Roboto-Bold", size: size+delatFontSize)!;
        return UIFont(name: "OpenSans-Bold", size: size+delatFontSize)!;
    }
    
    class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("--- Font Names = [\(names)]")
        }
    }
}
