//
//  FloatExtension.swift
//  ParKarma
//
//  Created by SOTSYS129 on 09/04/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation

extension Float {
    
    func roundToInt() -> Int{
        var value = Int(self)
        var f = self - Float(value)
        if f < 0.5{
            return value
        } else {
            return value + 1
        }
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
