//
//  StringExtension+PKValidation.swift.swift
//  ParKarma
//
//  Created by SOTSYS129 on 09/04/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import  UIKit
var Controller : UIViewController!

extension String{
    
    //MARK:- Signup validation
    func signupValidaion(toController: UIViewController,email: String,username : String, password: String,ConfPassword: String) -> Bool {
        
        Controller = toController
        //MARK:- SignUp Validation
        if emailValidaion(toController: toController, email: email) {
          
            if(SOValidator.required(username)){
                
                if ((username.length) > 2 && (username.length) < 32) {
                   
                    if passwordValidation(toController: toController, password: password){
                        
                        if(SOValidator.required(ConfPassword)){
                            
                            if(password == ConfPassword){
                                
                               return true
                                
                            }else{
                                objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NMatchPassword)
                                return false
                            }
                        }else{
                            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NConfirmPasswordEmpty)
                            return false
                        }
                    }else{
                        return false
                    }
                }else{
                    objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NUserNameLimit)
                    return false
                }
            }else{
                objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NUserNameEmpty)
                return false
            }
        }else{
            return false
      }
    }
    //MARK:- Login Validation
    func loginValidaion(toController: UIViewController,email: String, password: String) -> Bool {

        Controller = toController
        guard emailValidaion(toController: toController, email: email) else {
             return false
        }
        guard passwordValidation(toController: toController, password: password) else{
            return false
        }
        return true
    }
    //MARK:- ChangePassword Validation
    func changePasswordValidation(toController: UIViewController,oldPwd:String, newPwd: String, confPwd:String) -> Bool{
        
        guard SOValidator.required(oldPwd) else{
            
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NOldPasswordEmpty)
            return false
        }
        guard passwordValidation(toController: toController, password: newPwd) else{
            return false
        }
        guard (SOValidator.required(confPwd)) else{
            return false
        }
        guard newPwd == confPwd else{
            
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NMatchPassword)
            return false
        }
        return true
    }
    //MARK:- forgotPassword Validation
    func forgotPasswordValidation(toController: UIViewController,email: String) -> Bool{
       
        guard emailValidaion(toController: toController, email: email)else{
            return false
        }
        return true
    }
    func emailValidaion(toController: UIViewController,email: String) -> Bool {
        if (trim(string:email).count) > 0 {
            
            if(SOValidator.isEmail(email)){
                return true
            }
            else{
                objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NEmailValid)
                return false
            }
        }
        else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NEmailEmpty)
            return false
        }
    }
    func passwordValidation(toController: UIViewController,password: String) -> Bool
    {
        if(SOValidator.required(password))
        {
            if ((password.length) > PwdMinRange && (password.length) < PwdMaxRange){
                //&& objAppSingleton.isConatinsOneNumber(passwordString:password) && objAppSingleton.isConatinsOneCapitalLetter(passwordString: password)
                
                    return true
            }else{
                objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NInvalidPassword)
                return false
            }
        }else{
            objAppSingleton.showAlertView(title: APPNAME, msg: ErrorMessage.NPasswordEmpty)
            return false
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        
        if testStr.count > 0
        {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
        return true
    }
    func clearTextField(textField : [customeTextField])
    {
        for txt in textField
        {
            txt.text = ""
        }
    }
//    func showAlert(_ title : String = appName,  _ message:String){
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
//
//        }
//        alertController.addAction(cancelAction)
//        AppDelObj.window?.rootViewController!.present(alertController, animated: true, completion: nil)
//    }
    func trim(string: String) -> String
    {
        return string.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func replaceString(str : String) -> String
    {
        return str.replacingOccurrences(of: "$", with: "")
    }
}

