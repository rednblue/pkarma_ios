//
//  customeTextField.swift
//  ParKarma
//
//  Created by SOTSYS129 on 27/04/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit

class customeTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        customizeView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        customizeView()
    }
    func customizeView()
    {
        self.autocorrectionType = .no
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
