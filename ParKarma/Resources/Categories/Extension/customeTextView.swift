//
//  customeTextView.swift
//  ParKarma
//
//  Created by SOTSYS129 on 27/04/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit

class customeTextView: UITextView {

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        customizeView()
//    }
//    override init(frame: CGRect, textContainer: NSTextContainer?) {
//        super.init(frame: frame)
//         customizeView()
//    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        customizeView()
    }
    func customizeView(){
        
       self.autocorrectionType = .no
        self.layer.borderColor = UIColor(red: 241.0/255, green: 241.0/255, blue: 241.0/255, alpha: 1.0).cgColor
        self.layer.borderWidth = 1
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
