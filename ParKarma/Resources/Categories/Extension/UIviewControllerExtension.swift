//
//  UIviewControllerExtension.swift
//  ParKarma
//
//  Created by SOTSYS129 on 10/04/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension UIViewController{
    
    public func isLocationServiceEnabled() -> Bool {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            default:
                print("Something wrong with Location services")
                return false
            }
        } else {
            print("Location services are not enabled")
            return false
        }
    }
}

