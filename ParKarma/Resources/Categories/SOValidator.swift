//
//  Validator.swift
//  SwiftValidators
//
//  Created by Γιώργος Καϊμακάς on 7/22/15.
//  Copyright (c) 2015 Γιώργος Καϊμακάς. All rights reserved.
//

import Foundation

public typealias Validation = (String) -> Bool

public protocol ValidatorProtocol {
    func getValue() -> String
}

open class SOValidator {

    public enum ValidationMode {
        case `default`    // if set to Default validation succeeds for "" (emty string)
        case strict     // if set to Strict validation fails for "" (empty string)
    }

    open class FQDNOptions {
        open static let defaultOptions: FQDNOptions = FQDNOptions(requireTLD: true, allowUnderscores: false, allowTrailingDot: false)

        open let requireTLD: Bool
        open let allowUnderscores: Bool
        open let allowTrailingDot: Bool

        public init(requireTLD: Bool, allowUnderscores: Bool, allowTrailingDot: Bool) {
            self.requireTLD = requireTLD
            self.allowUnderscores = allowUnderscores
            self.allowTrailingDot = allowTrailingDot
        }
    }

    open static let defaultDateFormat: String = "dd/MM/yyyy"

    // Singleton with default values for easy use. For more configuration options
    // create a new instance
    fileprivate static let defaultValidator: SOValidator = SOValidator()

    fileprivate static let
    ΕmailRegex: String = "[\\w._%+-|]+@[\\w0-9.-]+\\.[A-Za-z]{2,6}",
    ΑlphaRegex: String = "[a-zA-Z]+",
    Βase64Regex: String = "(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?",
    CreditCardRegex: String = "(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})",
    HexColorRegex: String = "#?([0-9A-F]{3}|[0-9A-F]{6})",
    HexadecimalRegex: String = "[0-9A-F]+",
    ASCIIRegex: String = "[\\x00-\\x7F]+",
    NumericRegex: String = "[-+]?[0-9]+",
    FloatRegex: String = "([\\+-]?\\d+)?\\.?\\d*([eE][\\+-]\\d+)?",
    PhoneRegex: [String:String] = [
            "zh-CN": "(\\+?0?86\\-?)?1[345789]\\d{9}",
            "en-ZA": "(\\+?27|0)\\d{9}",
            "en-AU": "(\\+?61|0)4\\d{8}",
            "en-HK": "(\\+?852\\-?)?[569]\\d{3}\\-?\\d{4}",
            "fr-FR": "(\\+?33|0)[67]\\d{8}",
            "pt-PT": "(\\+351)?9[1236]\\d{7}",
            "el-GR": "(\\+30)?((2\\d{9})|(69\\d{8}))",
            "en-GB": "(\\+?44|0)7\\d{9}",
            "en-US": "(\\+?1)?[2-9]\\d{2}[2-9](?!11)\\d{6}",
            "en-ZM": "(\\+26)?09[567]\\d{7}",
            "ru-RU": "(\\+?7|8)?9\\d{9}"
    ],
    IPRegex: [String:String] = [
            "4": "(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})",
            "6": "[0-9A-Fa-f]{1,4}"
    ],
    ISBNRegex: [String:String] = [
            "10": "(?:[0-9]{9}X|[0-9]{10})",
            "13": "(?:[0-9]{13})"
    ],
    AlphanumericRegex: String = "[\\d[A-Za-z]]+",
    SocialSecurity: String = "^(\\d{3}-?\\d{2}-?\\d{4}|XXX-XX-XXXX)$"
    

    // Static validators the use the default configuration

    /**
    Checks if the seed contains the value
    
    - parameter seed:
    - returns: (String)->Bool
    */
    open static func contains(_ seed: String) -> Validation {
        return SOValidator.defaultValidator.contains(seed)
    }


    /**
    Checks if the seed is equal to the value
    
    - parameter seed:
    - returns: (String)->Bool
    */
    open static func equals(_ seed: String) -> Validation {
        return SOValidator.defaultValidator.equals(seed)
    }

    /**
    Checks if it is the exact length
    
    - parameter length:
    - returns: (String)->Bool
    */
    open static func exactLength(_ length: Int) -> Validation {
        return SOValidator.defaultValidator.exactLength(length)
    }


    /**
    checks if it is valid ascii string
    
    - returns: (String)->Bool
    */
    open static var isASCII: Validation {
        return SOValidator.defaultValidator.isASCII
    }

    /**
    checks if it is after the date
    
    - parameter
    - returns:: (String)->Bool
    */
    open static func isAfter(_ date: String) -> Validation {
        return SOValidator.defaultValidator.isAfter(date)
    }

    /**
    checks if it has only letters
    
    - returns: (String)->Bool
    */
    open static var isAlpha: Validation {
        return SOValidator.defaultValidator.isAlpha
    }

    /**
    checks if it has letters and numbers only
    
    - returns: (String)->Bool
    */
    open static var isAlphanumeric: Validation {
        return SOValidator.defaultValidator.isAlphanumeric
    }

    /**
    checks if it a valid base64 string
    
    - returns: (String)->Bool
    */
    open static var isBase64: Validation {
        return SOValidator.defaultValidator.isBase64
    }

    /**
    checks if it is before the date

    - parameter date: A date as a string
    - returns: (String)->Bool
    */
    open static func isBefore(_ date: String) -> Validation {
        return SOValidator.defaultValidator.isBefore(date)
    }

    /**
    checks if it is boolean
    
    - returns: (String)->Bool
    */
    open static var isBool: Validation {
        return SOValidator.defaultValidator.isBool
    }

    /**
    checks if it is a credit card number
    
    - returns: (String)->Bool
    */
    open static var isCreditCard: Validation {
        return SOValidator.defaultValidator.isCreditCard
    }

    /**
    checks if it is a valid date
    
    - returns: (String)->Bool
    */
    open static var isDate: Validation {
        return SOValidator.defaultValidator.isDate
    }

    /**
    checks if it is an email
    
    - returns: (String)->Bool
    */
    open static var isEmail: Validation {
        return SOValidator.defaultValidator.isEmail
    }

    open static var isSocialSecurity: Validation {
        return SOValidator.defaultValidator.isSocialSecurity
    }
    /**
    checks if it is an empty string
    
    - returns: (String)->Bool
    */
    open static var isEmpty: Validation {
        return SOValidator.defaultValidator.isEmpty
    }

    /**
    checks if it is fully qualified domain name

    - parameter options: An instance of FDQNOptions
    - returns: (String)->Bool
    */
    open static func isFQDN(_ options: FQDNOptions = FQDNOptions.defaultOptions) -> Validation {
        return SOValidator.defaultValidator.isFQDN(options)
    }

    /**
    checks if it is false
    
    - returns: (String)->Bool
    */
    open static var isFalse: Validation {
        return SOValidator.defaultValidator.isFalse
    }

    /**
    checks if it is a float number
    
    - returns: (String)->Bool
    */
    open static var isFloat: Validation {
        return SOValidator.defaultValidator.isFloat
    }

    /**
    checks if it is a valid hex color
    
    - returns: (String)->Bool
    */
    open static var isHexColor: Validation {
        return SOValidator.defaultValidator.isHexColor
    }

    /**
    checks if it is a hexadecimal value
    
    - returns: (String)->Bool
    */
    open static var isHexadecimal: Validation {
        return SOValidator.defaultValidator.isHexadecimal
    }

    /**
    checks if it is a valid IP (4|6)
    
    - returns: (String)->Bool
    */
    open static var isIP: Validation {
        return SOValidator.defaultValidator.isIP
    }

    /**
    checks if it is a valid IPv4
    
    - returns: (String)->Bool
    */
    open static var isIPv4: Validation {
        return SOValidator.defaultValidator.isIPv4
    }

    /**
    checks if it is a valid IPv6
    
    - returns: (String)->Bool
    */
    open static var isIPv6: Validation {
        return SOValidator.defaultValidator.isIPv6
    }

    /**
    checks if it is a valid ISBN

    - parameter version: ISBN version "10" or "13"
    - returns: (String)->Bool
    */
    open static func isISBN(_ version: String) -> Validation {
        return SOValidator.defaultValidator.isISBN(version)
    }

    /**
    checks if the value exists in the supplied array

    - parameter array: An array of strings
    - returns: (String)->Bool
    */
    open static func isIn(_ array: Array<String>) -> Validation {
        return SOValidator.defaultValidator.isIn(array)
    }

    /**
    checks if it is a valid integer
    
    - returns: (String)->Bool
    */
    open static var isInt: Validation {
        return SOValidator.defaultValidator.isInt
    }

    /**
    checks if it only has lowercase characters
    
    - returns: (String)->Bool
    */
    open static var isLowercase: Validation {
        return SOValidator.defaultValidator.isLowercase
    }

    /**
    checks if it is a hexadecimal mongo id
    
    - returns: (String)->Bool
    */
    open static var isMongoId: Validation {
        return SOValidator.defaultValidator.isMongoId
    }

    /**
    checks if it is numeric
    
    - returns: (String)->Bool
    */
    open static var isNumeric: Validation {
        return SOValidator.defaultValidator.isNumeric
    }

    /**
    checks if is is a valid phone
    
    - returns: (String)->Bool
    */
    open static func isPhone(_ locale: String) -> Validation {
        return SOValidator.defaultValidator.isPhone(locale)
    }

    /**
    checks if it is true
    
    - returns: (String)->Bool
    */
    open static var isTrue: Validation {
        return SOValidator.defaultValidator.isTrue
    }

    /**
    checks if it is a valid UUID
    
    - returns: (String)->Bool
    */
    open static var isUUID: Validation {
        return SOValidator.defaultValidator.isUUID
    }

    /**
    checks if has only uppercase letter
    
    - returns: (String)->Bool
    */
    open static var isUppercase: Validation {
        return SOValidator.defaultValidator.isUppercase
    }

    /**
    checks if the length does not exceed the max length

    - parameter length: The max length
    - returns: (String)->Bool
    */
    open static func maxLength(_ length: Int) -> Validation {
        return SOValidator.defaultValidator.maxLength(length)
    }

    /**
    checks if the length isn't lower than

    - parameter minLength: The min length
    - returns: (String)->Bool
    */
    open static func minLength(_ length: Int) -> Validation {
        return SOValidator.defaultValidator.minLength(length)
    }
    
    /**
     watch the validator protocol implementor for changes
     
     -parameter delegate: The validator protocol implementor
     - returns: (String)->Bool
    */
    open static func watch(_ delegate: ValidatorProtocol) -> Validation {
        return SOValidator.defaultValidator.watch(delegate)
    }

    /**
    checks if it is not an empty string
     
    - returns: (String)->Bool
    */
    open static var required: Validation {
        return SOValidator.defaultValidator.required
    }


    // ------------------------ //
    // ------------------------ //
    // ------------------------ //

    public init() {
        self.validationMode = .default
        self.dateFormatter.dateFormat = SOValidator.defaultDateFormat
    }

    public init(validationMode: ValidationMode) {
        self.validationMode = validationMode
        self.dateFormatter.dateFormat = SOValidator.defaultDateFormat
    }

    public init(validationMode: ValidationMode, dateFormat: String) {
        self.validationMode = validationMode
        self.dateFormatter.dateFormat = dateFormat
    }

    open let validationMode: ValidationMode
    open let dateFormatter = DateFormatter()


    /**
    Checks if the seed contains the value

    - parameter seed:
    - returns: (String)->Bool
    */
    open func contains(_ string: String) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value.range(of: string) != nil
        }
    }

    /**
    Checks if the seed is equal to the value

    - parameter seed:
    - returns: (String)->Bool
    */
    open func equals(_ string: String) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value == string
        }
    }

    /**
    checks if it has the exact length

    - parameter length:
    - returns: (String)->Bool
    */
    open func exactLength(_ length: Int) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value.count == length ? true : false
        }
    }
    
    /**
    checks if it is a valid ascii string
    
    - returns: (String)->Bool
    */
    open var isASCII: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.ASCIIRegex, value)
        }
    }

    /**
    checks if it is after the date

    - parameter date:
    - returns: (String)->Bool
    */
    open func isAfter(_ date: String) -> Validation {
        let startDate: Date? = self.dateFormatter.date(from: date)
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            let date: Date? = self.dateFormatter.date(from: value)
            if let _date = date {
                let comparison = _date.compare(startDate!)
                switch (comparison) {
                case ComparisonResult.orderedAscending:
                    return false
                case ComparisonResult.orderedSame:
                    return true
                case ComparisonResult.orderedDescending:
                    return true
                }
            } else {
                return false
            }

        }
    }
    
    /**
    checks if it has only letters
    
    - returns: (String)->Bool
    */
    open var isAlpha: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let test = NSPredicate(format: "SELF MATCHES %@", SOValidator.ΑlphaRegex)
            return test.evaluate(with: value)
        }
    }
    
    /**
    checks if it has letters and numbers only
    
    - returns: (String)->Bool
    */
    open var isAlphanumeric: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.AlphanumericRegex, value)
        }
    }
    
    /**
    checks if it a valid base64 string
    
    - returns: (String)->Bool
    */
    open var isBase64: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let test = NSPredicate(format: "SELF MATCHES %@", SOValidator.Βase64Regex)
            return test.evaluate(with: value)
        }
    }

    /**
    checks if it is before the date

    - parameter date: A date as a string
    - returns: (String)->Bool
    */
    open func isBefore(_ date: String) -> Validation {
        let startDate: Date? = self.dateFormatter.date(from: date)
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            let date: Date? = self.dateFormatter.date(from: value)
            if let _date = date {
                let comparison = _date.compare(startDate!)
                switch (comparison) {
                case ComparisonResult.orderedAscending:
                    return true
                case ComparisonResult.orderedSame:
                    return true
                case ComparisonResult.orderedDescending:
                    return false
                }
            } else {
                return false
            }

        }
    }
    
    /**
    checks if it is boolean
    
    - returns: (String)->Bool
    */
    open var isBool: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.isTrue(value) || self.isFalse(value)
        }
    }

    
    /**
    checks if it is a credit card number
    
    - returns: (String)->Bool
    */
    open var isCreditCard: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let test = NSPredicate(format: "SELF MATCHES %@", SOValidator.CreditCardRegex)
            var clearValue = self.removeDashes(value)
            clearValue = self.removeSpaces(clearValue)
            return test.evaluate(with: clearValue)
        }
    }

    
    /**
    checks if it is a valid date
    
    - returns: (String)->Bool
    */
    open var isDate: Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let date: Date? = self.dateFormatter.date(from: value)
            if let _ = date {
                return true
            } else {
                return false
            }

        }
    }

    
    /**
    checks if it is an email
    
    - returns: (String)->Bool
    */
    open var isEmail: Validation {
        return {
            (value: String) -> Bool in

            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let emailTest = NSPredicate(format: "SELF MATCHES %@", SOValidator.ΕmailRegex)
            return emailTest.evaluate(with: value)
        }
    }

    open var isSocialSecurity: Validation {
        return {
            (value: String) -> Bool in
            
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            
            let SocialSecurityTest = NSPredicate(format: "SELF MATCHES %@", SOValidator.SocialSecurity)
            return SocialSecurityTest.evaluate(with: value)
        }
    }
    
    /**
    checks if it is an empty string
    
    - returns: (String)->Bool
    */
    open var isEmpty: Validation {
        return {
            (value: String) in
            return value == ""
        }
    }

    /**
    checks if it is fully qualified domain name

    - parameter options: An instance of FDQNOptions
    - returns: (String)->Bool
    */
    open func isFQDN(_ options: FQDNOptions = FQDNOptions.defaultOptions) -> Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            var string = value
            
            if (options.allowTrailingDot && string.hasSuffix(".")) {
                string = string[0 ..< string.length]
            }
            
//            if (options.allowTrailingDot && string.lastCharacter == ".") {
//                string = string[0 ..< string.length]
//            }

            var parts = string.split(omittingEmptySubsequences: false) {
                $0 == "."
            }.map { String($0) }

            if (options.requireTLD) {
                let tld = parts.removeLast()
                if (parts.count == 0 || !self.regexTest("([a-z\u{00a1}-\u{ffff}]{2,}|xn[a-z0-9-]{2,})", tld) ){
                    return false
                }
            }

            for part in parts {
                var _part = part
                if (options.allowUnderscores) {
                    if (self.regexTest("__", _part)) {
                        return false
                    }
                }
                _part = self.removeUnderscores(_part)

                if (!self.regexTest("[a-z\u{00a1}-\u{ffff0}-9-]+", _part)){
                    return false
                }
                
                if (_part[0] == "-" || _part.hasSuffix("-") || self.regexTest("---", _part)) {
                    return false
                }

//                if (_part[0] == "-" || _part.lastCharacter == "-" || self.regexTest("---", _part)) {
//                    return false
//                }
            }

            return true
        }
    }

    
    /**
    checks if it is false
    
    - returns: (String)->Bool
    */
    open var isFalse: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            return value.lowercased() == "false"
        }
    }

    
    /**
    checks if it is a float number
    
    - returns: (String)->Bool
    */
    open var isFloat: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.FloatRegex, value)
        }
    }

    
    /**
    checks if it is a valid hex color
    
    - returns: (String)->Bool
    */
    open var isHexColor: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let test = NSPredicate(format: "SELF MATCHES %@", SOValidator.HexColorRegex)
            let newValue = value.uppercased()
            let result = test.evaluate(with: newValue)
            return result
        }
    }

    
    /**
    checks if it is a hexadecimal value
    
    - returns: (String)->Bool
    */
    open var isHexadecimal: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            let newValue = value.uppercased()
            return self.regexTest(SOValidator.HexadecimalRegex, newValue)
        }
    }

    
    /**
    checks if it is a valid IP (v4 or v6)
    
    - returns: (String)->Bool
    */
    open var isIP: Validation {
        return {
            (value: String) in
            return self.isIPv4(value) || self.isIPv6(value)
        }
    }

    
    /**
    checks if it is a valid IPv4
    
    - returns: (String)->Bool
    */    open var isIPv4: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.IPRegex["4"]!, value)
        }
    }

    
    /**
    checks if it is a valid IPv6
    
    - returns: (String)->Bool
    */
    open var isIPv6: Validation {
        return {
            (value: String) in
            let string: String = self.removeDashes(self.removeSpaces(value))

            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            var blocks = string.split(omittingEmptySubsequences: false) {
                $0 == ":"
            }.map { String($0) }

            var foundOmissionBlock = false // marker to indicate ::

            // At least some OS accept the last 32 bits of an IPv6 address
            // (i.e. 2 of the blocks) in IPv4 notation, and RFC 3493 says
            // that '::ffff:a.b.c.d' is valid for IPv4-mapped IPv6 addresses,
            // and '::a.b.c.d' is deprecated, but also valid.
            let validator: SOValidator = SOValidator(validationMode: .strict)
            let foundIPv4TransitionBlock = (blocks.count > 0 ? validator.isIPv4(blocks[blocks.count - 1]) : false)
            let expectedNumberOfBlocks = (foundIPv4TransitionBlock ? 7 : 8)

            if (blocks.count > expectedNumberOfBlocks) {
                return false
            }

            if (string == "::") {
                return true
            } else if (String( describing: string.index(string.startIndex, offsetBy: 2)) == "::") {
                blocks.remove(at: 0)
                blocks.remove(at: 0)
                foundOmissionBlock = true
            } else if (String( describing:String(Array(string.reversed())).index(string.startIndex, offsetBy: 2)) == "::") {
                blocks.removeLast()
                blocks.removeLast()
                foundOmissionBlock = true
            }

            for i in 0 ..< blocks.count {
                if (blocks[i] == "" && i > 0 && i < blocks.count - 1) {
                    if (foundOmissionBlock) {
                        return false
                    }
                    foundOmissionBlock = true
                } else if (foundIPv4TransitionBlock && i == blocks.count - 1) {

                } else if (!self.regexTest(SOValidator.IPRegex["6"]!, blocks[i])) {
                    return false
                }
            }

            if (foundOmissionBlock) {
                return blocks.count >= 1
            } else {
                return blocks.count == expectedNumberOfBlocks
            }
        }
    }
    
    /**
    checks if it is a valid ISBN

    - parameter version: ISBN version "10" or "13"
    - returns: (String)->Bool
    */
    open func isISBN(_ version: String) -> Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            var sanitized = self.removeDashes(value)
            sanitized = self.removeSpaces(sanitized)
            let regexTest: Bool = self.regexTest(SOValidator.ISBNRegex[version]!, sanitized)
            if (regexTest == false) {
                return false
            }

            var checksum: Int = 0
            if (version == "10") {

                for i in 0 ..< 9 {
                    checksum += (i + 1) * Int("\(sanitized[sanitized.index(sanitized.startIndex, offsetBy: i)])")!
                }

                if ("\(sanitized[sanitized.index(sanitized.startIndex, offsetBy: 9)])".lowercased() == "x") {
                    checksum += 10 * 10
                } else {
                    checksum += 10 * Int("\(sanitized[sanitized.index(sanitized.startIndex, offsetBy: 9)])")!
                }

                if (checksum % 11 == 0) {
                    return true
                }

            } else if (version == "13") {
                var factor = [1, 3]
                for i in 0 ..< 12 {
                    let charAt: Int = Int("\(sanitized[sanitized.index(sanitized.startIndex, offsetBy: i)])")!
                    checksum += factor[i % 2] * charAt
                }

                let charAt12 = Int("\(sanitized[sanitized.index(sanitized.startIndex, offsetBy: 12)])")!
                if ((charAt12 - ((10 - (checksum % 10)) % 10)) == 0) {
                    return true
                }
            }

            return false
        }
    }
    
    /**
    checks if the value exists in the supplied array

    - parameter array: An array of strings
    - returns: (String)->Bool
    */
    open func isIn(_ array: Array<String>) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            if let _ = array.index(of: value) {
                return true
            } else {
                return false
            }
        }
    }

    
    /**
    checks if it is a valid integer
    
    - returns: (String)->Bool
    */
    open var isInt: Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.isNumeric(value)
        }
    }

    
    /**
    checks if it only has lowercase characters
    
    - returns: (String)->Bool
    */
    open var isLowercase: Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            return value == value.lowercased()
        }
    }

    
    /**
    checks if it is a hexadecimal mongo id
    
    - returns: (String)->Bool
    */
    open var isMongoId: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.isHexadecimal(value) && value.count == 24
        }
    }

    
    /**
    checks if it is numeric
    
    - returns: (String)->Bool
    */
    open var isNumeric: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.NumericRegex, value)
        }
    }
    
    /**
    checks if is is a valid phone

    - parameter locale: The locale as a String. Available locales are 'zh-CN', 'en-ZA', 'en-AU', 'en-HK', 'pt-PT', 'fr-FR', 'el-GR', 'en-GB', 'en-US', 'en-ZM', 'ru-RU
    - returns: (String)->Bool
    */
    open func isPhone(_ locale: String) -> Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return self.regexTest(SOValidator.PhoneRegex[locale]!, value)
        }
    }
    
    
    /**
    checks if it is true
    
    - returns: (String)->Bool
    */
    open var isTrue: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            return value.lowercased() == "true"
        }
    }

    
    /**
    checks if it is a valid UUID
    
    - returns: (String)->Bool
    */
    open var isUUID: Validation {
        return {
            (value: String) in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            let uuid = UUID(uuidString: value)
            if let _ = uuid {
                return true
            }

            return false

        }
    }

    
    /**
    checks if it has only uppercase letters
    
    - returns: (String)->Bool
    */
    open var isUppercase: Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }

            return value == value.uppercased()
        }
    }
    
    /**
    checks if the length does not exceed the max length

    - parameter length: The max length
    - returns: (String)->Bool
    */
    open func maxLength(_ length: Int) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value.count <= length ? true : false
        }
    }
    
    /**
    checks if the length isn't lower than

    - parameter length: The min length
    - returns: (String)->Bool
    */
    open func minLength(_ length: Int) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value.count >= length ? true : false
        }
    }

    
    /**
    checks if it is not an empty string

    - returns: (String)->Bool
    */
    open var required: Validation {
        return {
            (value: String) in
            return value != ""
        }
    }
    
    /**
     watch the validator protocol implementor for changes
     
     -parameter delegate: The validator protocol implementor
     - returns: (String)->Bool
     */
    open func watch(_ delegate: ValidatorProtocol) -> Validation {
        return {
            (value: String) -> Bool in
            if value == "" {
                return (self.validationMode == .default ? true : false)
            }
            return value == delegate.getValue()
        }
    }


    // ------------------------ //
    // ------------------------ //
    // ------------------------ //

    fileprivate func removeSpaces(_ value: String) -> String {
        return self.removeCharacter(value, char: " ")
    }

    fileprivate func removeDashes(_ value: String) -> String {
        return self.removeCharacter(value, char: "-")
    }

    fileprivate func removeUnderscores(_ value: String) -> String {
        return self.removeCharacter(value, char: "_")
    }

    fileprivate func removeCharacter(_ value: String, char: String) -> String {
        return value.replacingOccurrences(of: char, with: "")
    }

    fileprivate func regexTest(_ regex: String, _ value: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: value)
    }
}

