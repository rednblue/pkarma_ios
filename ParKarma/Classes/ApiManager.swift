//
//  ApiManager.swift
//  ParKarma
//
//  Created by SOTSYS022 on 08/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import Moya

class ApiManager: NSObject {
    
    static var instance : ApiManager!
    
    class func sharedInstance() -> ApiManager
    {
        instance = (self.instance ?? ApiManager())
        return instance
    }
    
}


