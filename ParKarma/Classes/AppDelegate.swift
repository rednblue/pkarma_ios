//
//  AppDelegate.swift
//  ParKarma
//
//  Created by SOTSYS022 on 30/01/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Reachability
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
//import FLEX

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UIGestureRecognizerDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Enable IQKeyboardManager
        IQKeyboardManager.shared().isEnabled =  true
        
        //Enable Reachability
        let objReachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(note:)), name: Notification.Name.reachabilityChanged, object: objReachability)
        do {
            try objReachability?.startNotifier()
        } catch  {
            print("Reachablity could not started...")
        }
      
        //#if DEBUG
          //  FLEXManager.shared().showExplorer()
        //#endif

        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(gesture:)))
        longPress.delegate = self
        longPress.numberOfTouchesRequired = 2
        
        
        //Enable Google Map
        GMSServices.provideAPIKey("AIzaSyAr3EO56nDHMxFVySDLWVPWDYVAmhTTPnc")
        GMSPlacesClient.provideAPIKey("AIzaSyAr3EO56nDHMxFVySDLWVPWDYVAmhTTPnc")

        //Enable Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        
        objAppSingleton.additionalFontSize = objAppSingleton.getAdditionalFontSize()

        print("AdditionalFontSize => \(objAppSingleton.additionalFontSize)")
        
        let attrs = [
            NSAttributedStringKey.font: UIFont(name: "OpenSans-SemiBold", size: 17)!
        ]
        UINavigationBar.appearance().titleTextAttributes = attrs
        
        UserDM.sharedInstance = UserDM.lastLoggingUser()
        if(UserDM.sharedInstance != nil)
        {
            if(UserDM.sharedInstance.userID != 0)
            {
            let objUINavigationController = UINavigationController()
            let objParkingSpotViewController = HOME_STORYBOARD.instantiateViewController(withIdentifier: ParkingSpotViewController.className) as! ParkingSpotViewController
            objUINavigationController.viewControllers = [objParkingSpotViewController]
            objAppDelegate.window?.rootViewController = objUINavigationController
            }
        }
        return true
    }
    @objc func handleLongPressGesture(gesture:UILongPressGestureRecognizer){
        
//        #if DEBUG
//                FLEXManager.shared().showExplorer()
//        #endif
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //MARK:- REACHABILITY DELEGATE
    @objc func reachabilityChanged(note:Notification)
    {
        let objReachability = note.object as! Reachability
        
        switch objReachability.connection {
        case .wifi:
            print("Connected via wifi...")
            objAppSingleton.isNetworkAvailable = true
        case .cellular:
            print("Connected via cellular...")
            objAppSingleton.isNetworkAvailable = true
        case .none:
            print("Internet not available...")
            objAppSingleton.isNetworkAvailable = false
        }
    }
   
}

