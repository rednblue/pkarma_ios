//
//  Constants.swift
//  ParKarma
//
//  Created by SOTSYS022 on 01/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import UIKit

let APPNAME = "ParKarma"

extension NSObject
{
    var className : String {
        
        return String(describing: self)
    }
    class var className : String {
        
        return String(describing: self)
    }
}
//create obj of AppSingleton
let objAppSingleton = AppSingleton.sharedInstance()

let NLoadingViewTag = 55555
let TextViewMaxChar = 140
let PwdMinRange = 5
let PwdMaxRange = 32
//Appdelegate shared object
let objAppDelegate : AppDelegate = AppDelegate().sharedInstance()

//MARK: - StoryBorad Name
let MAIN_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let HOME_STORYBOARD = UIStoryboard(name: "Home", bundle: nil)

//MARK:- Colors
let kSKYCOLOR = UIColor.init(red: 2/255, green: 225/255, blue: 225/255, alpha: 1.0)

struct ErrorMessage{
    
    static let NEmailEmpty = "Email address can not be blank."
    static let NEmailValid = "Invalid Email Address."
    static let NInvalidPassword = "Password must be 6-32 characters long."//, and include at least one capital letter, and a number.
    static let NAtleastOneNumber = "Password must have at least one number."
    static let NAtleastOneCapitalLetter = "Password must have at least one capital letter."
    static let NPasswordEmpty = "Password can not be blank."
    static let FacebookValidation = "Sorry! cannot be access, this is private account."
    static let registered  = "We've created your account. Please click OK to log in."
    static let NUserNameEmpty = "User Name can not be blank."
    static let NUserNameLimit = "Username must be 3-32 characters long."
    static let NConfirmPasswordEmpty = "Confirm Password can not be blank."
    static let NMatchPassword = "Password & confirm password must be same."
    static let NNewPasswordEmpty = "New Password can not be blank."
    static let NOldPasswordEmpty = "Old Password can not be blank."
    static let NETWORKALERT = "There is no internet connection at this moment, please try again later."
    static let NAddSpotNoteEmpty = "Parking note can not be blank."
    static let NParkingReportedSuccess = "Parking sport reported successfully."
}
struct AlertMessage{
    
    static let locationPermission = "Your current location isn’t showing up on the map. If you turn on the location service, the app will find a location for you."
}
struct LabelDefaultText {

   static let textViewPlaceHolder = "Type your message here       "
   static let feedbackPlaceHolder = "Ask or tell us anything"
}

// MARK: - userDefaults
class Constant: NSObject {
    
    static var apiKey: String? {
        set{
            guard let unwrappedKey = newValue else{
                return
            }
            UserDefaults.standard.set(unwrappedKey,forKey: "DeviceToken")
            UserDefaults.standard.synchronize()
        }get{
            return UserDefaults.standard.value(forKey: "DeviceToken") as? String
        }
    }
    static var milesValue: Float? {
        set{
            guard let unwrappedKey = newValue else{
                return
            }
            UserDefaults.standard.set(unwrappedKey,forKey: "milesValue")
            UserDefaults.standard.synchronize()
        }get{
            return UserDefaults.standard.value(forKey: "milesValue") as? Float
        }
    }
}



