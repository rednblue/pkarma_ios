//
//  AppSingleton.swift
//  ParKarma
//
//  Created by SOTSYS022 on 01/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import UIKit
import DeviceKit

class AppSingleton: NSObject {
    
    static var instance : AppSingleton!
    
    let deviceType : Device = Device()
    
    let deviceWith_4_0_Inch: [Device] = [.iPhoneSE,.iPhone5, .iPhone5c, .iPhone5s]
    let deviceWith_4_7_Inch: [Device] = [.iPhone6, .iPhone6s, .iPhone7]
    let deviceWith_5_5_Inch: [Device] = [.iPhone6Plus, .iPhone6sPlus, .iPhone7Plus]
    let deviceWith_5_8_Inch: [Device] = [.iPhoneX]
    
    var additionalFontSize : Int!
    var isNetworkAvailable : Bool!
    
    class func sharedInstance() -> AppSingleton {
        self.instance = (self.instance ?? AppSingleton())
        return self.instance
    }
    
    //MARK:- UIView Fade Animation
      func setView(view: UIView,subView: UIView, hidden: Bool) {
        
       
        UIView.animate(withDuration: 0.2, animations: {
            view.isHidden = hidden
            subView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            
        }) { (finished) in
            UIView.animate(withDuration: 0.2, animations: {
             subView.transform = CGAffineTransform.identity
             
            })
        }
    }
    func hideView(view: UIView,hidden: Bool = true){
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
                        view.isHidden = hidden
         }, completion: nil)
    }
    //MARK:- Globle Alert
    func showAlertView(title : String, msg : String){
        let alert: UIAlertController = UIAlertController(title: title , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(okAction)
        objAppDelegate.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    func showAlert(_ baseModel : BaseDM){
        if baseModel.responseMessage.count == 0 {
            return;
        }
        let alertController = UIAlertController(title: "", message: baseModel.responseMessage, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
        }
        alertController.addAction(cancelAction)
        objAppDelegate.window?.rootViewController!.present(alertController, animated: true, completion: nil)
    }
    func goToLoginScreen(){
        UserDM.removeFromLocal()
        let objLoginViewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: LoginViewController.className) as! LoginViewController
        let navController : UINavigationController = UINavigationController(rootViewController: objLoginViewController)
        // navController.addChildViewController(objLoginViewController)
        navController.navigationBar.isHidden = true
        objAppDelegate.window?.rootViewController = navController
        Constant.apiKey = nil
    }
    //MARK:- Check Device Type
    func getAdditionalFontSize() -> Int {
        if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_4_0_Inch) {
            return 0
        }
        else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_4_7_Inch) {
            return 2
        }
        else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_5_5_Inch) {
            return 4
        }
        else if objAppSingleton.deviceType.isOneOf(objAppSingleton.deviceWith_5_8_Inch) {
            return 6
        }
        else
        {
            return 0
        }
    }
    
    //MARK:- Check Internet
    func isNetwork() -> Bool {
        return isNetworkAvailable
    }
    //MARK: Start/Stop Loading indicator in view
    func startLoading() {
        let objAJProgressView = AJProgressView()
        // Pass your image here which will come in center of ProgressView
        
        
        objAJProgressView.imgLogo = UIImage(named:"AppLogo")!
        objAJProgressView.tag = NLoadingViewTag

        // Pass the color for the layer of progressView
        objAJProgressView.firstColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // If you  want to have layer of animated colors you can also add second and third color
        objAJProgressView.secondColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        objAJProgressView.thirdColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // Set duration to control the speed of progressView
        objAJProgressView.duration = 2.0
        
        // Set width of layer of progressView
        objAJProgressView.lineWidth = 4.0
        
        //Set backgroundColor of progressView
        objAJProgressView.bgColor =  UIColor.black.withAlphaComponent(0.3)
        
        //Get the status of progressView, if view is animating it will return true.
        _ = objAJProgressView.isAnimating
        
        //Use show() and hide() to manage progressView
        objAJProgressView.show()

    }
    func stopLoading() {
        let viewLoading = objAppDelegate.window?.viewWithTag(NLoadingViewTag) as! AJProgressView
        viewLoading.hide()
    }
    func getPinColor(colorId:Int) -> UIColor {
        switch colorId {
        case 1:
            return UIColor(red: 0.0/255, green: 227.0/255, blue: 254.0/255, alpha: 1.0)  // 00E3FE 0, 227, 254
        case 2:
            return UIColor(red: 0.0/255, green: 124.0/255, blue: 230.0/255, alpha: 1.0) //007CE6 0  124 230
        case 3:
            return UIColor(red: 18.0/255, green: 18.0/255, blue: 18.0/255, alpha: 1.0) //121212 18 18 18
        default:
            return .black
        }
    }
    func getMsgColor(colorId:Int) -> UIColor {
        switch colorId {
        case 1:
            return UIColor(red: 18.0/255, green: 18.0/255, blue: 18.0/255, alpha: 1.0) //121212 18 18 18 black
        case 2:
            return UIColor(red: 0.0/255, green: 227.0/255, blue: 254.0/255, alpha: 1.0)  // 00E3FE 0, 227, 254 skyBlue
        case 3:
            return UIColor(red: 0.0/255, green: 124.0/255, blue: 230.0/255, alpha: 1.0) //007CE6 0  124 230 blue
        default:
            return .black
        }
    }
    
    func isConatinsOneNumber(passwordString:String)-> Bool
    {
        return self.matches(for: "[0-9]", in: passwordString).count>0 ? true : false
    }
    func isConatinsOneCapitalLetter(passwordString:String)-> Bool
    {
        return self.matches(for: "[A-Z]", in: passwordString).count>0 ? true : false
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
}
