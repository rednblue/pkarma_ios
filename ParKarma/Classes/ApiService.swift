//
//  ApiService.swift
//  ParKarma
//
//  Created by SOTSYS022 on 08/02/18.
//  Copyright © 2018 SOTSYS022. All rights reserved.
//

import Foundation
import Moya
enum ApiService
{
    case SignUp(user:String,email:String,password:String,firstName:String,lastName:String)
    case Login(email:String,password:String)
    case ForgotPassword(email:String)
    case ChangePassword(email:String,oldPassword:String,newPassword:String)
    case getNearByPins(user_id: Int,latitude: Double, longitude: Double,icon:Int,distance:Float)
    case reportParkingPin(user_id: Int,latitude: Double, longitude: Double,icon:Int,distance:Int,msg:String)

}
extension ApiService: TargetType {
    
    struct Parameter {
        static let RESULT = "data"
        static let RESPONSE_STATUS = "success"
        static let MESSAGE = "msg"
        static let USER = "user"
        static let EMAIL = "email"
        static let PASSWORD = "password"
        static let OLDPASSWORD = "oldPassword"
        static let NEWPASSWORD = "newPassword"
        static let USERID = "userid"
        static let LATITUDE = "latitude"
        static let LONGITUDE = "longitude"
        static let ICON = "icon"
        static let DISTANCE = "distance"
        static let FIRSTNAME = "firstName"
        static let LASTNAME = "lastName"
        static let API_KEY = "api_key"
    }
    
    var baseURL: URL { return URL(string: "https://pkarma-test.herokuapp.com")! }

    var path: String {
        switch self {
        case .SignUp:
            return "/register"
        case .Login:
            return "/login"
        case .ForgotPassword:
            return "/resetPasswordFromApp"
        case .ChangePassword:
            return "/changePassword"
        case .getNearByPins:
            return "/pins"
        case .reportParkingPin:
            return "/pins"
        
        }
        
    }
    var method: Moya.Method {
        switch self {
        case .SignUp,.Login,.ForgotPassword,.ChangePassword,.reportParkingPin:
            return .post
        case .getNearByPins:
            return .get
        }
    }
    var task: Task {
        
        var deviceToken:String = "WyJyZWRuYmx1ZUBnbWFpbC5jb20iXQ.vRKJVhQFc2BLgSCZIONvTDlmfVE" //you-wil-never-guess-api-key
        
        if (Constant.apiKey != nil)
        {
            deviceToken = Constant.apiKey!
            print("device token -->",deviceToken)
        }
        switch self {
            
        case let .SignUp(user, email,password,firstName,lastName):
            return .requestParameters(parameters: [ApiService.Parameter.USER: user, ApiService.Parameter.EMAIL: email,ApiService.Parameter.PASSWORD: password,ApiService.Parameter.FIRSTNAME: firstName,ApiService.Parameter.LASTNAME: lastName,ApiService.Parameter.API_KEY: deviceToken], encoding: JSONEncoding.default)
            
        case let .Login(email,password):
            return .requestParameters(parameters: [ ApiService.Parameter.EMAIL: email,ApiService.Parameter.PASSWORD: password,ApiService.Parameter.API_KEY: deviceToken], encoding: JSONEncoding.default)
            
        case let .ForgotPassword(email):
            return .requestParameters(parameters: [ ApiService.Parameter.EMAIL: email,ApiService.Parameter.API_KEY: deviceToken], encoding: JSONEncoding.default)
            
        case let .ChangePassword(email,oldPassword,newPassword):
            return .requestParameters(parameters: [ ApiService.Parameter.EMAIL: email,ApiService.Parameter.OLDPASSWORD: oldPassword,ApiService.Parameter.NEWPASSWORD: newPassword,ApiService.Parameter.API_KEY: deviceToken], encoding: JSONEncoding.default)
            
        case let .getNearByPins(user_id, latitude, longitude, icon, distance):
             return .requestParameters(parameters: [ApiService.Parameter.USERID: user_id,ApiService.Parameter.LATITUDE: latitude,ApiService.Parameter.LONGITUDE: longitude,ApiService.Parameter.ICON: icon,ApiService.Parameter.DISTANCE: distance,ApiService.Parameter.API_KEY: deviceToken], encoding: URLEncoding.default)
            
        case let .reportParkingPin(user_id, latitude, longitude, icon, distance,msg):
            return .requestParameters(parameters: [ApiService.Parameter.USERID: user_id,ApiService.Parameter.LATITUDE: latitude,ApiService.Parameter.LONGITUDE: longitude,ApiService.Parameter.ICON: icon,ApiService.Parameter.DISTANCE: distance,ApiService.Parameter.MESSAGE: msg,ApiService.Parameter.API_KEY: deviceToken], encoding: JSONEncoding.default)
        }
    }
    var sampleData: Data {
        switch self {        
        case .SignUp(let user,let email,let password,let firstName,let lastName):
            return "{\"user\": \"\(user)\", \"email\": \"\(email)\",\"password\": \"\(password)\", \"firstName\": \"\(firstName)\",\"lastName\": \"\(lastName)\"}".utf8Encoded
        case .Login(let email,let password):
            return "{ \"email\": \"\(email)\",\"password\": \"\(password)\"}".utf8Encoded
        case .ForgotPassword(let email):
            return "{ \"email\": \"\(email)\"}".utf8Encoded
        case .ChangePassword (let email,let oldPassword,let newPassword):
            return "{ \"email\": \"\(email)\",\"oldPassword\": \"\(oldPassword)\",\"newPassword\": \"\(newPassword)\"}".utf8Encoded
        case .getNearByPins(let user_id, let latitude, let longitude, let icon, let distance):
              return "{ \"user_id\": \"\(user_id)\",\"latitude\": \"\(latitude)\",\"longitude\": \"\(longitude)\", \"icon\": \"\(icon)\", \"distance\": \"\(distance)\"}".utf8Encoded
        case .reportParkingPin(let user_id, let latitude, let longitude, let icon, let distance, let msg):
            return "{ \"user_id\": \"\(user_id)\",\"latitude\": \"\(latitude)\",\"longitude\": \"\(longitude)\", \"icon\": \"\(icon)\", \"distance\": \"\(distance)\", \"msg\": \"\(msg)\"}".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
